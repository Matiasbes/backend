﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Image
{
    public class ImageDefinition
    {

        public int Id { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FileName { get; set; }

    }
}
