﻿using BusinessLogic.Authentication;
using BusinessLogic.Image;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users
{
    public class UsersController
    {
        public UsersDataProvider DataProvider { get; set; }

        public virtual User GetUser(User user)
        {
            if (user.IsNull())
                throw new ArgumentException("Se debe indicar un usuario");
            return DataProvider.GetUser(user);
        }
        public virtual User GetUserWithCredentials(string email, string password)
        {
            if (!EmailValidator.IsValid(email))
                throw new ArgumentException("El email no es válido");
            if (password.NullOrEmpty())
                throw new ArgumentException("El usuario debe tener una contraseña");
            string encryptedPassword = GeneratePasswordHash(password);
            return DataProvider.GetUserWithCredentials(email, encryptedPassword);
        }

        public virtual User AddUser(User user)
        {
            ValidateUser(user);
            user.Password = GeneratePasswordHash(user.Password);
            return DataProvider.AddUser(user);
        }
        public virtual int GetUserRating(User user)
        {
            if (user.IsNull())
                throw new ArgumentException("Se debe indicar un usuario");
            return DataProvider.GetUserRating(user);
        }
        private void ValidateUser(User user)
        {
            if (user.IsNull())
                throw new ArgumentException("Se debe indicar el usuario a agregar");
            if (user.Name.NullOrEmpty())
                throw new ArgumentException("El usuario debe tener un nombre");
            if (!EmailValidator.IsValid(user.Email))
                throw new ArgumentException("El email no es válido");
            if (user.Password.NullOrEmpty())
                throw new ArgumentException("El usuario debe tener una contraseña");
            if (DataProvider.ExistsUserWithEmail(user.Email))
                throw new ArgumentException("Ya existe un usuario registrado con el email");
        }

        public string GeneratePasswordHash(string password)
        {
            using (var sha = new MD5CryptoServiceProvider())
            {
                var passwordBytes = Encoding.UTF8.GetBytes(password);
                return string.Concat(sha.ComputeHash(passwordBytes).Select(x => x.ToString("X2")));
            }
        }
        public virtual bool ExistsUserWithEmail(string email)
        {
            if (!EmailValidator.IsValid(email))
                throw new ArgumentException("El email no es válido");
            return DataProvider.ExistsUserWithEmail(email);
        }
        public virtual User ChangePassword(User user, string actualPassword, string newPassword)
        {
            if (user.IsNull())
                throw new ArgumentException("Se debe indicar el usuario a modificar");
            if (newPassword.NullOrEmpty())
                throw new ArgumentException("Se debe indicar una contraseña");

            User realUser = DataProvider.GetUser(user);
            if (!GeneratePasswordHash(actualPassword).Equals(realUser.Password))
                throw new ArgumentException("La actual contraseña no es correcta");
            newPassword = GeneratePasswordHash(newPassword);
            return DataProvider.ChangePassword(user, newPassword);
        }

        public virtual Supplier GetSupplierFromUser(User user)
        {
            return DataProvider.GetSupplierFromUser(user);
        }

        public virtual Client GetClientFromUser(User user)
        {
            return DataProvider.GetClientFromUser(user);
        }


        public virtual Session NewSessionForUser(User user)
        {
            Session session = Session.Generate();
            DataProvider.AddSessionToUser(user, session);
            return session;
        }
        public virtual User GetUserFromSession(Session session)
        {
            return DataProvider.GetUserFromSession(session);
        }

        public virtual void RemoveSession(Session session)
        {
            DataProvider.RemoveSession(session);
        }

        public virtual User GetUserWithFacebookId(string facebookId)
        {
            if (facebookId.IsNull())
                throw new ArgumentException("Se debe proveer la identificación de Facebook");
            return DataProvider.GetUserWithFacebookId(facebookId);
        }
        public virtual User SyncUserWithFacebook(string userEmail, string facebookId)
        {
            return DataProvider.SyncUserWithFacebook(userEmail, facebookId);
        }

        public virtual User AddUserSyncedWithFacebook(User user)
        {
            ValidateFacebookUser(user);
            return DataProvider.AddUser(user);
        }

        private void ValidateFacebookUser(User user)
        {
            if (user.IsNull())
                throw new ArgumentException("Se debe indicar el usuario a agregar");
            if (user.Name.NullOrEmpty())
                throw new ArgumentException("El usuario debe tener un nombre");
            if (!EmailValidator.IsValid(user.Email))
                throw new ArgumentException("El email no es válido");
            if (DataProvider.ExistsUserWithEmail(user.Email))
                throw new ArgumentException("Ya existe un usuario registrado con el email");
            if (user.FacebookId.IsNull())
                throw new ArgumentException("Se debe proveer la identificación de Facebook");
        }

        public virtual void ModifyProfilePhoto(User user, ImageDefinition profilePhoto)
        {
            DataProvider.ModifyProfilePhoto(user, profilePhoto);
        }

        public void AddDeviceId(User user, DeviceId deviceId)
        {
            if (deviceId.IsNull() || deviceId.DeviceToken.NullOrEmpty())
                return;
            if (user.IsNull())
                throw new ArgumentException("Se debe indicar el usuario");
            DataProvider.AddDeviceId(user, deviceId);
        }
    }
}
