﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users.Suppliers.Degrees
{
    public class DegreesController
    {
        public DegreesDataProvider DataProvider { get; set; }

        public virtual IList<Degree> GetDegreesForSupplier(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier para el cual obtener sus certificaciones");
            return DataProvider.GetDegreesForSupplier(supplier);
        }

        public virtual Degree GetDegree(Degree degree)
        {
            if (degree.IsNull())
                throw new ArgumentException("Se debe indicar la certificación a obtener");
            return DataProvider.GetDegree(degree);
        }

        public virtual Degree AddDegree(Degree degree, Supplier supplier)
        {
            ValidateDegree(degree);
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier poseedor de la certificación");
            return DataProvider.AddDegree(degree, supplier);
        }

        private void ValidateDegree(Degree degree)
        {
            if (degree.IsNull())
                throw new ArgumentException("Se debe indicar la certificación");
            if (degree.Institution.NullOrEmpty())
                throw new ArgumentException("Se debe indicar la institución");
            if (degree.StartDate.EqualOrGreaterThan(degree.EndDate))
                throw new ArgumentException("La fecha de inicio de la certificación debe ser menor que la fecha de fin");
            if (degree.StartDate.EqualOrGreaterThan(DateTime.Today))
                throw new ArgumentException("La fecha de inicio de la certificación debe ser menor que la fecha actual");
            if (degree.Title.NullOrEmpty())
                throw new ArgumentException("Se debe indicar la certificación");
        }

        public virtual Degree ModifyDegree(Degree degreeToModify, Degree newDegree)
        {
            if (degreeToModify.IsNull())
                throw new ArgumentException("Se debe indicar la certificación a modificar");
            ValidateDegree(newDegree);
            return DataProvider.ModifyDegree(degreeToModify, newDegree);
        }

        public virtual void RemoveDegree(Degree degree)
        {
            if (degree.IsNull())
                throw new ArgumentException("Se debe indicar la certificación a eliminar");
            DataProvider.RemoveDegree(degree);
        }
    }
}
