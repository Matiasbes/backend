﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers.Degrees
{
    public interface DegreesDataProvider
    {
        IList<Degree> GetDegreesForSupplier(Supplier supplier);
        Degree GetDegree(Degree degree);
        Degree AddDegree(Degree degree, Supplier supplier);
        Degree ModifyDegree(Degree degreeToModify, Degree newDegree);
        void RemoveDegree(Degree degree);
    }
}
