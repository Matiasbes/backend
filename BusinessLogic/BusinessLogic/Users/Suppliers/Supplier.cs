﻿using BusinessLogic.Projects;
using BusinessLogic.Users.Suppliers.Degrees;
using BusinessLogic.Users.Suppliers.Jobs;
using BusinessLogic.Users.Suppliers.Portfolio;
using BusinessLogic.Users.Suppliers.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers
{
    public class Supplier : User
    {
        public float? Rate { get; set; }
        public string Overview { get; set; }
        public virtual IList<Degree> Degrees { get; set; }
        public virtual IList<Job> Jobs { get; set; }
        public virtual IList<Skill> Skills { get; set; }
        public virtual IList<PortfolioSample> Portfolio { get; set; }

        public Supplier()
        {
            Projects = new List<Project>();
            Degrees = new List<Degree>();
            Jobs = new List<Job>();
            Skills = new List<Skill>();
            Portfolio = new List<PortfolioSample>();
        }

    }
}
