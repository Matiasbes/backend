﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers.Skills
{
    public interface SkillsDataProvider
    {
        IList<Skill> GetSkillsForSupplier(Supplier supplier);
        Skill GetSkill(Skill skill);
        Skill AddSkill(Skill skill, Supplier supplier);
        Skill ModifySkill(Skill skillToModify, Skill newSkill);
        void RemoveSkill(Skill skill);
    }
}
