﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users.Suppliers.Skills
{
    public class SkillsController
    {
        public SkillsDataProvider DataProvider { get; set; }

        public virtual IList<Skill> GetSkillsForSupplier(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier para el cual obtener sus skills");
            return DataProvider.GetSkillsForSupplier(supplier);
        }

        public virtual Skill GetSkill(Skill skill)
        {
            if (skill.IsNull())
                throw new ArgumentException("Se debe indicar el skill a obtener");
            return DataProvider.GetSkill(skill);
        }

        public virtual Skill AddSkill(Skill skill, Supplier supplier)
        {
            ValidateSkill(skill);
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier al cual agregar el skill");
            return DataProvider.AddSkill(skill, supplier);
        }

        private void ValidateSkill(Skill skill)
        {
            if (skill.IsNull())
                throw new ArgumentException("Se debe indicar el skill a agregar");
            if (skill.Title.NullOrEmpty())
                throw new ArgumentException("El skill debe tener un título");
        }

        public virtual Skill ModifySkill(Skill skillToModify, Skill newSkill)
        {
            if (skillToModify.IsNull())
                throw new ArgumentException("Se debe indicar el skill a modificar");
            ValidateSkill(newSkill);
            return DataProvider.ModifySkill(skillToModify, newSkill);
        }

        public virtual void RemoveSkill(Skill skill)
        {
            if (skill.IsNull())
                throw new ArgumentException("Se debe indicar el skill a eliminar");
            DataProvider.RemoveSkill(skill);
        }

    }
}
