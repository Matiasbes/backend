﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users.Suppliers.Jobs
{
    public class JobsController
    {
        public JobsDataProvider DataProvider { get; set; }

        public virtual IList<Job> GetJobsForSupplier(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier para el cual obtener su historial laboral");
            return DataProvider.GetJobsForSupplier(supplier);
        }

        public virtual Job GetJob(Job job)
        {
            if (job.IsNull())
                throw new ArgumentException("Se debe indicar el trabajo a obtener");
            return DataProvider.GetJob(job);
        }

        public virtual Job AddJob(Job job, Supplier supplier)
        {
            ValidateJob(job);
            if (supplier.IsNull())
                throw new ArgumentException("Se debe inidicar el supplier que trabajó en dicha empresa");
            return DataProvider.AddJob(job, supplier);
        }

        private void ValidateJob(Job job)
        {
            if (job.IsNull())
                throw new ArgumentException("Se debe indicar un trabajo");
            if (job.Enterprise.NullOrEmpty())
                throw new ArgumentException("Se debe indicar la empresa donde trabajó");
            if (job.StartDate.EqualOrGreaterThan(job.EndDate))
                throw new ArgumentException("La fecha de comienzo del trabajo debe ser menor a la fecha de fin");
            if (job.StartDate.EqualOrGreaterThan(DateTime.Today))
                throw new ArgumentException("La fecha de comienzo del trabajo debe ser menor a la actual");
            if (job.Title.NullOrEmpty())
                throw new ArgumentException("Se debe indicar el título del trabajo");
        }

        public virtual Job ModifyJob(Job jobToModify, Job newJob)
        {
            if (jobToModify.IsNull())
                throw new ArgumentException("Se debe indicar el trabajo a modificar");
            ValidateJob(newJob);
            return DataProvider.ModifyJob(jobToModify, newJob);
        }

        public virtual void RemoveJob(Job job)
        {
            if (job.IsNull())
                throw new ArgumentException("Se debe indicar el trabajo a eliminar");
            DataProvider.RemoveJob(job);
        }
    }
}
