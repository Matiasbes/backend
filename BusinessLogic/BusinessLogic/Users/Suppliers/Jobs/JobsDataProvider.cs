﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers.Jobs
{
    public interface JobsDataProvider
    {
        IList<Job> GetJobsForSupplier(Supplier supplier);
        Job GetJob(Job job);
        Job AddJob(Job job, Supplier supplier);
        Job ModifyJob(Job jobToModify, Job newJob);
        void RemoveJob(Job job);
    }
}
