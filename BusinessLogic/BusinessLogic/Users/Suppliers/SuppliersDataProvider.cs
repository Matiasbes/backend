﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers
{
    public interface SuppliersDataProvider
    {
        bool ExistsSupplier(Supplier supplier);
        Supplier GetSupplier(Supplier supplier);
        void ModifySupplier(Supplier supplierToModify, Supplier newSupplier);
    }
}
