﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users.Suppliers
{
    public class SuppliersController
    {
        public SuppliersDataProvider DataProvider { get; set; }

        public virtual Supplier GetSupplier(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier a obtener");
            if (!DataProvider.ExistsSupplier(supplier))
                throw new ArgumentException("El supplier indicado no existe");
            return DataProvider.GetSupplier(supplier);
        }

        public virtual void ModifySupplier(Supplier supplierToModify, Supplier newSupplier)
        {
            if (supplierToModify.IsNull())
                throw new ArgumentException("Se debe indicar el supplier a modificar");
            ValidateSupplier(newSupplier);
            if (!DataProvider.ExistsSupplier(supplierToModify))
                throw new ArgumentException("El supplier a modificar no existe");
            DataProvider.ModifySupplier(supplierToModify, newSupplier);
        }

        private void ValidateSupplier(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier");
            if (supplier.Name.NullOrEmpty())
                throw new ArgumentException("Se debe indicar un nombre");
            if (supplier.Overview.NullOrEmpty())
                throw new ArgumentException("Se debe indicar la descripción personal");
            if (supplier.Rate < 0)
                throw new ArgumentException("El precio por hora debe ser mayor que cero");
        }
    }
}
