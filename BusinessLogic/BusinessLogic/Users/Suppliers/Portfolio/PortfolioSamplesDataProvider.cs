﻿using BusinessLogic.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers.Portfolio
{
    public interface PortfolioSamplesDataProvider
    {
        IList<PortfolioSample> GetPortfolioSamplesForSupplier(Supplier supplier);
        PortfolioSample GetPortfolioSample(PortfolioSample portfolioSample);
        PortfolioSample AddPortfolioSample(PortfolioSample portfolioSample, Supplier supplier);
        PortfolioSample ModifyPortfolioSample(PortfolioSample portfolioSampleToModify, PortfolioSample newPortfolioSample);
        void RemovePortfolioSample(PortfolioSample portfolioSample);
        void ModifyPhoto(PortfolioSample portfolioSample, ImageDefinition photo);
    }
}
