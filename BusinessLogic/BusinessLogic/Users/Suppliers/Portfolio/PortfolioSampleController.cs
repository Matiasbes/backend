﻿using BusinessLogic.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users.Suppliers.Portfolio
{
    public class PortfolioSamplesController
    {

        public PortfolioSamplesDataProvider DataProvider { get; set; }

        public virtual IList<PortfolioSample> GetPortfolioSamplesForSupplier(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier para el cual obtener el portfolio");
            return DataProvider.GetPortfolioSamplesForSupplier(supplier);
        }


        public virtual PortfolioSample GetPortfolioSample(PortfolioSample portfolioSample)
        {
            if (portfolioSample.IsNull())
                throw new ArgumentException("Se debe incluir la muestra de trabajo");
            return DataProvider.GetPortfolioSample(portfolioSample);
        }

        public virtual PortfolioSample AddPortfolioSample(PortfolioSample portfolioSample, Supplier supplier)
        {
            ValidatePortfolioSample(portfolioSample);
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier al cual pertenece la muestra de trabajo");
            return DataProvider.AddPortfolioSample(portfolioSample, supplier);
        }

        private void ValidatePortfolioSample(PortfolioSample portfolioSample)
        {
            if (portfolioSample.IsNull())
                throw new ArgumentException("Se debe indicar la muestra de trabajo a agregar");
            if (portfolioSample.Title.NullOrEmpty())
                throw new ArgumentException("Se debe indicar el titulo de la muestra de trabajo");
            if (portfolioSample.Description.NullOrEmpty())
                throw new ArgumentException("Se debe indicar la descripcion de la muestra de trabajo");
            if (portfolioSample.Date.EqualOrGreaterThan(DateTime.Today))
                throw new ArgumentException("La fecha de realización del trabajo debe ser anterior a la actual");
        }

        public virtual PortfolioSample ModifyPortfolioSample(PortfolioSample portfolioSampleToModify, PortfolioSample newPortfolioSample)
        {
            if (portfolioSampleToModify.IsNull())
                throw new ArgumentException("Se debe indicar la muestra de trabajo a modificar");
            ValidatePortfolioSample(newPortfolioSample);
            return DataProvider.ModifyPortfolioSample(portfolioSampleToModify, newPortfolioSample);
        }

        public virtual void RemovePortfolioSample(PortfolioSample portfolioSample)
        {
            if (portfolioSample.IsNull())
                throw new ArgumentException("Se debe indicar la muestra de trabajo a eliminar");
            DataProvider.RemovePortfolioSample(portfolioSample);
        }

        public virtual void ModifyPhoto(PortfolioSample portfolioSample, ImageDefinition photo)
        {
            if (portfolioSample.IsNull())
                throw new ArgumentException("Se debe indicar la muestra de trabajo");
            DataProvider.ModifyPhoto(portfolioSample, photo);
        }
    }

}
