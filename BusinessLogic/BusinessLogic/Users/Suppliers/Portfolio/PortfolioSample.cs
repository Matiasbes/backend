﻿using BusinessLogic.Image;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Suppliers.Portfolio
{
    public class PortfolioSample
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }
        public virtual ImageDefinition Photo { get; set; }

        public PortfolioSample()
        {
            Date = DateTime.Today;
        }

    }
}
