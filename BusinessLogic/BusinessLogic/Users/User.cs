﻿using BusinessLogic.Authentication;
using BusinessLogic.Image;
using BusinessLogic.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }

        public virtual string FacebookId { get; set; }
        public virtual IList<Project> Projects { get; set; }
        public virtual IList<Session> Sessions { get; set; }
        public virtual IList<DeviceId> DevicesIds { get; set; }

        public virtual ImageDefinition ProfilePhoto { get; set; }

        public User()
        {
            Projects = new List<Project>();
            Sessions = new List<Session>();
            DevicesIds = new List<DeviceId>();
        }

        public override bool Equals(Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            User p = (User)obj;
            return (this.Id == p.Id);
        }
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
