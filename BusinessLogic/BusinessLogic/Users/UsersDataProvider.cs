﻿using BusinessLogic.Authentication;
using BusinessLogic.Image;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users
{
    public interface UsersDataProvider
    {
        bool ExistsUserWithEmail(string email);
        User GetUserWithCredentials(string email, string password);
        User AddUser(User user);
        User GetUser(User user);
        User ChangePassword(User user, string newPassword);
        int GetUserRating(User user);

        void AddSessionToUser(User user, Session session);
        User GetUserFromSession(Session session);
        void RemoveSession(Session session);

        Supplier GetSupplierFromUser(User user);
        Client GetClientFromUser(User user);

        User GetUserWithFacebookId(string facebookId);
        User SyncUserWithFacebook(string userEmail, string facebookId);

        void ModifyProfilePhoto(User user, ImageDefinition profilePhoto);

        void AddDeviceId(User user, DeviceId deviceId);
    }
}
