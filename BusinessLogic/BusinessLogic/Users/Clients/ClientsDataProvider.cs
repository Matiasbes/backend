﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Users.Clients
{
    public interface ClientsDataProvider
    {
        bool ExistsClient(Client client);
        Client GetClient(Client client);
        void ModifyClient(Client clientToModify, Client newClient);
    }
}
