﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Users.Clients
{
    public class ClientsController
    {
        public ClientsDataProvider DataProvider { get; set; }

        public virtual Client GetClient(Client client)
        {
            if (client.IsNull())
                throw new ArgumentException("Se debe indicar un cliente");
            if (!DataProvider.ExistsClient(client))
                throw new ArgumentException("El cliente no existe");
            return DataProvider.GetClient(client);
        }

        public virtual void ModifyClient(Client clientToModify, Client newClient)
        {
            if (clientToModify.IsNull())
                throw new ArgumentException("Se debe indicar el cliente a modificar");
            if (newClient.IsNull())
                throw new ArgumentException("No se indicaron datos a modificar");
            if (!DataProvider.ExistsClient(clientToModify))
                throw new ArgumentException("El cliente no existe");
            if (newClient.Name.NullOrEmpty())
                throw new ArgumentException("El nombre no puede estar vacío");
            DataProvider.ModifyClient(clientToModify, newClient);
        }
    }
}
