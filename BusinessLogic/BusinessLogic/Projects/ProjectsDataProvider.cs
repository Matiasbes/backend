﻿using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Image;

namespace BusinessLogic.Projects
{
    public interface ProjectsDataProvider
    {
        List<Project> GetSupplierProjects(Supplier supplier);
        List<Project> GetClientProjects(Client client);
        List<Project> GetProjects();
        List<Project> GetProjectsByDistance(Double latitude, Double longitude, Double distance);
        Project AddProject(Project project);
        bool ExistsProject(Project project);
        Project GetProject(Project project);
        void ModifyProject(Project projectToModify, Project newProject);
        void FinalizeProject(Project project, IList<ProjectRating> supplierRatings);
        void RateClient(Supplier supplier, Project project, int rating);
        void ProjectsApplay(Supplier supplier, IList<Project> projects);
        void ModifyPhoto(Project project, ImageDefinition photo);
    }
}
