﻿using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Projects
{
    public class ProjectRating
    {
        public int Id { get; set; }
        public virtual Supplier Supplier { get; set; }
        public int SupplierRate { get; set; }
        public int ClientRate { get; set; }
    }
}
