﻿using BusinessLogic.Image;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BusinessLogic.Projects
{
    public class ProjectsController
    {
        private static int MINIMUM_RATE = 1;
        private static int MAXIMUM_RATE = 5;

        public ProjectsDataProvider DataProvider { get; set; }

        public virtual List<Project> GetSupplierProjects(Supplier supplier)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe indicar el supplier del cual desea obtener sus proyectos");
            return DataProvider.GetSupplierProjects(supplier);
        }

        public virtual List<Project> GetClientProjects(Client client)
        {
            if (client.IsNull())
                throw new ArgumentException("Se debe indicar el cliente del cual desea obtener sus proyectos");
            return DataProvider.GetClientProjects(client);
        }

        public List<Project> GetProjectsByDistance(Double latitude, Double longitude, Double distance, Supplier supplier)
        {
            if (latitude.IsNull() || longitude.IsNull())
                throw new ArgumentException("Se debe indicar la ubicacion");
            if (distance.IsNull())
                throw new ArgumentException("Se debe indicar la distancia deseada");
            return DataProvider.GetProjectsByDistance(latitude,longitude, distance).FindAll(p => (p.Status == ProjectStatus.Pending || p.Status == ProjectStatus.InProcess) && !p.Suppliers.Contains(supplier)); ;
        }

        public virtual Project GetProject(Project project)
        {
            if (project == null)
                throw new ArgumentException("Se debe indicar el proyecto a obtener");
            if (!DataProvider.ExistsProject(project))
                throw new ArgumentException("El proyecto indicado no existe");
            return DataProvider.GetProject(project);
        }

        public virtual Project AddProject(Project project)
        {
            ValidateProject(project);
            if (DataProvider.ExistsProject(project))
                throw new ArgumentException("El proyecto a agregar ya existe");
            return DataProvider.AddProject(project);
        }

        public virtual void ModifyProject(Project projectToModify, Project newProject)
        {
            if (projectToModify.IsNull())
                throw new ArgumentException("Se debe indicar el proyecto a modificar");
            ValidateProject(newProject);
            if (!DataProvider.ExistsProject(projectToModify))
                throw new ArgumentException("El proyecto a modificar no existe");
            DataProvider.ModifyProject(projectToModify, newProject);
        }

        private void ValidateProject(Project project)
        {
            if (project.IsNull())
                throw new ArgumentException("Se debe indicar el proyecto");
            if (project.Title.NullOrEmpty())
                throw new ArgumentException("Se debe indicar un nombre");
            if (project.Description.NullOrEmpty())
                throw new ArgumentException("Se debe indicar la descripción del proyecto");
            if (project.Budget < 0)
                throw new ArgumentException("El presupuesto del proyecto debe ser mayor a cero");
            if (project.Deadline.Equals(new DateTime()))
                throw new ArgumentException("Se debe indicar la fecha límite del proyecto");
            if (project.Deadline <= DateTime.Now && hasProjectFinallized(project))
                throw new ArgumentException("La fecha límite del proyecto debe ser en el futuro");
            if (project.Suppliers.Count == 0 && hasProjectStarted(project))
                throw new ArgumentException("Un proyecto que ha comenzado debe tener suppliers asignados");
        }

        private bool hasProjectStarted(Project project)
        {
            return  project.Status == ProjectStatus.InProcess;
        }

        private bool hasProjectFinallized(Project project)
        {
            return project.Status != ProjectStatus.Finished;
        }

        public virtual void FinalizeProject(Project project, IList<ProjectRating> supplierRatings)
        {
            Project realProject = GetProject(project);
            if (realProject.Suppliers.Count != supplierRatings.Count)
                throw new ArgumentException("Se deben proveer todas las calificaciones de los suppliers");
            if (!realProject.Suppliers.All(supplier => supplierRatings.Any(rating => rating.Supplier.Id == supplier.Id
                    && rating.SupplierRate.Between(MINIMUM_RATE, MAXIMUM_RATE))))
                throw new ArgumentException("No todos los suppliers poseen calificaciones");
            DataProvider.FinalizeProject(realProject, supplierRatings);
        }

        public virtual void RateClient(Supplier supplier, Project project, int rating)
        {
            if (supplier.IsNull())
                throw new ArgumentException("El supplier no puede ser nulo");
            Project realProject = DataProvider.GetProject(project);
            if (realProject.Status != ProjectStatus.Finished)
                throw new ArgumentException("El proyecto no está finalizado");
            if (!realProject.Suppliers.Any(theSupplier => theSupplier.Id == supplier.Id))
                throw new ArgumentException("El supplier no formó parte del proyecto");
            if (realProject.Ratings.First(rate => rate.Supplier.Id == supplier.Id).ClientRate >= MINIMUM_RATE)
                throw new ArgumentException("El supplier ya calificó al cliente");
            if (!rating.Between(MINIMUM_RATE, MAXIMUM_RATE))
                throw new ArgumentException("La calificacion esta fuera del rango");
            DataProvider.RateClient(supplier, realProject, rating);
        }

        public List<Project> GetAvailableProjects(Supplier supplier)
        {
            return DataProvider.GetProjects().FindAll(p => (p.Status == ProjectStatus.Pending || p.Status == ProjectStatus.InProcess) && !p.Suppliers.Contains(supplier));
        }


        public virtual void ProjectsApplay(Supplier supplier, IList<Project> projects)
        {
            if (supplier.IsNull())
                throw new ArgumentException("Se debe inidicar el supplier");
            if (projects.IsNull() || projects.Count() == 0)
                throw new ArgumentException("Se debe seleccionar por lo menos un projecto");
            DataProvider.ProjectsApplay(supplier, projects);
        }

        public virtual void ModifyPhoto(Project project, ImageDefinition photo)
        {
            if (project.IsNull())
                throw new ArgumentException("Se debe indicar la muestra de trabajo");
            DataProvider.ModifyPhoto(project, photo);
        }
    }
}
