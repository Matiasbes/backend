﻿using BusinessLogic.Image;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Projects
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ProjectStatus Status { get; set; }
        public float Budget { get; set; }
        public DateTime Deadline { get; set; }
        public virtual ImageDefinition Photo { get; set; }
        public virtual ProjectLocation Location { get; set; }

        public virtual Client Client { get; set; }
        public virtual IList<Supplier> Suppliers { set; get; }
        public virtual IList<ProjectRating> Ratings { get; set; }

        public Project()
        {
            Suppliers = new List<Supplier>();
            Ratings = new List<ProjectRating>();
        }
    }
}
