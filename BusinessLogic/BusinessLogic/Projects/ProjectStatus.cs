﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Projects
{
    public enum ProjectStatus
    {
        [Description("Pending")]
        Pending,

        [Description("InProcess")]
        InProcess,

        [Description("Finished")]
        Finished
    }
}
