﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Authentication
{
    public class Session
    {

        public int Id { get; set; }
        public string Token { get; set; }

        public static Session Generate()
        {
            Session session = new Session();
            session.Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            return session;
        }

    }
}
