﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Authentication
{
    public class DeviceId
    {

        public int Id { get; set; }
        public string DeviceToken { get; set; }

    }
}
