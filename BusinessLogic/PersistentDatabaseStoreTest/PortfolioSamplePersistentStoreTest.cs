﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Portfolio;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class PortfolioSamplePersistentStoreTest
    {

        private Mock<PersistentStoreContext> GetPersistentStoreContext()

        {
            var mockContext = new Mock<PersistentStoreContext>();

            mockContext.Setup(mock => mock.PortfolioSamples).Returns(GetDbSetMock<PortfolioSample>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);
            mockSet.Setup(mock => mock.Remove(It.IsAny<T>())).Callback<T>(obj => container.Remove(obj));

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        [Fact]
        public void TestAddPortfolioSampleSupplierNotExists()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            PortfolioSample Portfolio = new PortfolioSample() { Id = 1, Title = "Sample" };
            Assert.Throws<ArgumentException>(() => persistentStore.AddPortfolioSample(Portfolio, Supplier));
        }

        [Fact]
        public void TestAddPortfolioSample()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            PortfolioSample Portfolio = new PortfolioSample() { Id = 1, Title = "Sample" };
            persistentStore.AddPortfolioSample(Portfolio, Supplier);

            PortfolioSample PortfolioRecieved = persistentStore.PersistentStoreContext.Suppliers.First().Portfolio.First();
            Assert.Equal(Portfolio, PortfolioRecieved);
        }

        [Fact]
        public void TestGetPortfolioSampleNoPortfolioSample()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            PortfolioSample Portfolio = new PortfolioSample() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetPortfolioSample(Portfolio));
        }

        [Fact]
        public void TestGetPortfolioSamplePortfolioSampleNotFound()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            PortfolioSample Portfolio = new PortfolioSample() { Id = 204 };
            persistentStore.PersistentStoreContext.PortfolioSamples.Add(Portfolio);

            PortfolioSample PortfolioSampleToFind = new PortfolioSample() { Id = 10 };
            Assert.Throws<ArgumentException>(() => persistentStore.GetPortfolioSample(PortfolioSampleToFind));
        }

        [Fact]
        public void TestGetPortfolioSampleFromSupplierSupplierNotExists()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            PortfolioSample Portfolio = new PortfolioSample() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetPortfolioSamplesForSupplier(Supplier));
        }

        [Fact]
        public void TestGetPortfolioSampleFromSupplier()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            PortfolioSample Portfolio = new PortfolioSample() { Id = 1, Title = "Sample" };
            PortfolioSample Portfolio2 = new PortfolioSample() { Id = 20, Title = "Sample2" };
            persistentStore.AddPortfolioSample(Portfolio, Supplier);
            persistentStore.AddPortfolioSample(Portfolio2, Supplier);

            IList<PortfolioSample> PortfoliosExpected = new List<PortfolioSample> { Portfolio, Portfolio2 };

            Assert.Equal(PortfoliosExpected, persistentStore.GetPortfolioSamplesForSupplier(Supplier));
        }

        [Fact]
        public void TestModifyPortfolioSample()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            PortfolioSample Portfolio = new PortfolioSample() { Id = 204, Title = "Title1" };
            persistentStore.PersistentStoreContext.PortfolioSamples.Add(Portfolio);

            PortfolioSample NewPortfolio = new PortfolioSample() { Id = 204, Title = "Title2" };

            persistentStore.ModifyPortfolioSample(Portfolio, NewPortfolio);

            PortfolioSample changedPortfolio = persistentStore.GetPortfolioSample(new PortfolioSample() { Id = 204 });

            Assert.Equal(NewPortfolio.Title, changedPortfolio.Title);
        }

        [Fact]
        public void TestRemovePortfolioSample()
        {
            PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            PortfolioSample Portfolio = new PortfolioSample() { Id = 204, Title = "Title1" };
            persistentStore.PersistentStoreContext.PortfolioSamples.Add(Portfolio);

            PortfolioSample existingPortfolio = persistentStore.GetPortfolioSample(Portfolio);

            Assert.Equal(Portfolio.Title, existingPortfolio.Title);

            persistentStore.RemovePortfolioSample(Portfolio);

            Assert.Throws<ArgumentException>(() => persistentStore.GetPortfolioSample(Portfolio));
        }

    }

}
