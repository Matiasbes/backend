﻿using BusinessLogic.Users;
using BusinessLogic.Authentication;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Clients;

namespace PersistentDatabaseStoreTest
{
    public class UserPersistentStoreTests
    {

        [Fact]
        public void TestAddUser()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 20 };
            persistentStore.AddUser(User);

            User UserRecieved = persistentStore.GetUser(User);
            Assert.Equal(User, UserRecieved);
        }

        private Mock<PersistentStoreContext> GetPersistentStoreContext()
        {   var mockContext = new Mock<PersistentStoreContext>();

            mockContext.Setup(mock => mock.Users).Returns(GetDbSetMock<User>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);
            mockContext.Setup(mock => mock.Clients).Returns(GetDbSetMock<Client>().Object);
            mockContext.Setup(mock => mock.Sessions).Returns(GetDbSetMock<Session>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);
            mockSet.Setup(mock => mock.Remove(It.IsAny<T>())).Callback<T>(obj => container.Remove(obj));

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        [Fact]
        public void TestExistsUserWithEmailNoExists()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            string email = "fercastagno@gmail.com";
            User User = new User() { Email = email };
            persistentStore.AddUser(User);

            Assert.False(persistentStore.ExistsUserWithEmail(email + "mmm"));
        }

        [Fact]
        public void TestExistsUserWithEmailExists()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            string email = "fercastagno@gmail.com";
            User User = new User() { Email = email };
            persistentStore.AddUser(User);

            Assert.True(persistentStore.ExistsUserWithEmail(email));
        }

        [Fact]
        public void TestRemoveSession()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Session Session = Session.Generate();

            persistentStore.PersistentStoreContext.Sessions.Add(Session);

            Assert.Equal(persistentStore.PersistentStoreContext.Sessions.First(), Session);

            persistentStore.RemoveSession(Session);

            Assert.Equal(persistentStore.PersistentStoreContext.Sessions.Count(), 0);

        }

        [Fact]
        public void TestGetUserNoUsers()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetUser(User));
        }

        [Fact]
        public void TestGetUserUserNotFound()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204 };
            persistentStore.AddUser(User);

            User UserToFind = new User() { Id = 10 };
            Assert.Throws<ArgumentException>(() => persistentStore.GetUser(UserToFind));
        }

        [Fact]
        public void TestGetUser()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            int UserId = 125;

            User User = new User() { Id = UserId };
            persistentStore.AddUser(User);

            User UserToFind = new User() { Id = UserId };
            Assert.Equal(User, persistentStore.GetUser(UserToFind));
        }

        [Fact]
        public void TestGetUserWithCredentialsUserNotFound()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204, Email = "fercastagno@gmail.com", Password = "Password123" };
            persistentStore.AddUser(User);

            Assert.Throws<ArgumentException>(() => persistentStore.GetUserWithCredentials("matiasbesozzi9@gmail.com", "Password123"));
        }

        [Fact]
        public void TestGetUserWithCredentials()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204, Email = "fercastagno@gmail.com", Password = "Password123" };
            persistentStore.AddUser(User);

            Assert.Equal(User, persistentStore.GetUserWithCredentials("fercastagno@gmail.com", "Password123"));
        }

        [Fact]
        public void TestChangePassword()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204, Email = "fercastagno@gmail.com", Password = "Password123" };
            persistentStore.AddUser(User);

            persistentStore.ChangePassword(User, "123Password");

            User retirevedUser = persistentStore.GetUser(new User() { Id = 204 });
            Assert.Equal("123Password", retirevedUser.Password);
        }

        [Fact]
        public void TestAddSessionToUser()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204, Email = "fercastagno@gmail.com", Password = "Password123" };
            persistentStore.AddUser(User);

            Session Session = Session.Generate();
            persistentStore.AddSessionToUser(User, Session);

            User retrivedUser = persistentStore.GetUserFromSession(Session);
            Assert.Equal(User, retrivedUser);
        }

        [Fact]
        public void TestGetSupplierFromUserNotFound()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(supplier);

            User user = new User() { Id = 40, Email = "fercastagno@gmail.com", Password = "Password123" };

            Assert.Throws<ArgumentException>(() => persistentStore.GetSupplierFromUser(user));
        }



        [Fact]
        public void TestGetSupplierFromUser()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(supplier);

            User user = new User() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Supplier retreivedSupplier = persistentStore.GetSupplierFromUser(user);
            Assert.Equal(supplier, retreivedSupplier);
        }


        [Fact]
        public void TestGetClientFromUserNotFound()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(client);

            User user = new User() { Id = 40, Email = "fercastagno@gmail.com", Password = "Password123" };

            Assert.Throws<ArgumentException>(() => persistentStore.GetClientFromUser(user));

        }

        [Fact]
        public void TestGetClientFromUser()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(client);

            User user = new User() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Client retreivedClient = persistentStore.GetClientFromUser(user);
            Assert.Equal(client, retreivedClient);

        }

        [Fact]
        public void TestGetUserWithFacebookIdNoUsers()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Assert.Throws<FacebookUserNotFoundException>(() => persistentStore.GetUserWithFacebookId("facebookId"));
        }

        [Fact]
        public void TestGetUserWithFacebookIdUserNotFound()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204, FacebookId = "facebook" };
            persistentStore.AddUser(User);

            Assert.Throws<FacebookUserNotFoundException>(() => persistentStore.GetUserWithFacebookId("facebookId"));
        }

        [Fact]
        public void TestGetUserWithFacebookId()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 200, FacebookId = "facebook" };
            persistentStore.AddUser(User);

            Assert.Equal(User, persistentStore.GetUserWithFacebookId("facebook"));
        }

        [Fact]
        public void TestSyncUserWithFacebookNoUsers()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Assert.Throws<ArgumentException>(() => persistentStore.SyncUserWithFacebook("fercastagno@gmail.com", "facebook"));
        }

        [Fact]
        public void TestSyncUserWithFacebookUserNotFound()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 204, Email = "fercastagno@hotmail.com" };
            persistentStore.AddUser(User);

            Assert.Throws<ArgumentException>(() => persistentStore.SyncUserWithFacebook("fercastagno@gmail.com", "facebook"));
        }

        [Fact]
        public void TestSyncUserWithFacebook()
        {
            UserPersistentStore persistentStore = new UserPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            User User = new User() { Id = 200, Email = "fercastagno@gmail.com" };
            persistentStore.AddUser(User);
            persistentStore.SyncUserWithFacebook("fercastagno@gmail.com", "facebook");

            User userObtained = persistentStore.GetUserWithFacebookId("facebook");

            Assert.Equal(User.Email, userObtained.Email);
        }
    }
}
