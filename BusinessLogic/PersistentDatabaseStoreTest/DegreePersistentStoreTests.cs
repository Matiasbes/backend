﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Degrees;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class DegreePersistentStoreTests
    {
        private Mock<PersistentStoreContext> GetPersistentStoreContext()

        {
            var mockContext = new Mock<PersistentStoreContext>();

            mockContext.Setup(mock => mock.Degrees).Returns(GetDbSetMock<Degree>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);
            mockSet.Setup(mock => mock.Remove(It.IsAny<T>())).Callback<T>(obj => container.Remove(obj));

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        private Mock<PersistentStoreContext> GetPersistentStoreContextDatabaseConnectionProblem()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Degrees).Returns(GetDbSetMockDatabaseConnectionProblem<Degree>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMockDatabaseConnectionProblem<Supplier>().Object);


            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMockDatabaseConnectionProblem<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Throws<InvalidOperationException>();
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Throws<InvalidOperationException>();

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Throws<InvalidOperationException>();

            return mockSet;
        }

        [Fact]
        public void TestAddDegreeDatabaseConnectionProblem()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Degree Degree = new Degree() { Id = 20 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.AddDegree(Degree, Supplier));
        }

        [Fact]
        public void TestAddDegreeSupplierNotExists()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Degree Degree = new Degree() { Id = 20 };

            Assert.Throws<ArgumentException>(() => persistentStore.AddDegree(Degree,Supplier));
        }

        [Fact]
        public void TestAddDegree()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Degree Degree = new Degree() { Id = 20 };
            persistentStore.AddDegree(Degree, Supplier);

            persistentStore.PersistentStoreContext.Degrees.Add(Degree);

            Degree DegreeRecieved = persistentStore.GetDegree(Degree);
            Assert.Equal(Degree, DegreeRecieved);
        }

        [Fact]
        public void TestGetDegreesDatabaseConnectionProblem()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetDegreesForSupplier(Supplier));
        }

        [Fact]
        public void TestGetDegreesForSupplierWithZeroDegrees()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Assert.Equal(new List<Degree>(), persistentStore.GetDegreesForSupplier(Supplier));
        }

        [Fact]
        public void TestGetDegreesForSupplierNotExistsSupplier()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };
            Assert.Throws<ArgumentException>(() => persistentStore.GetDegreesForSupplier(Supplier));
        }

        [Fact]
        public void TestGetDegreesForSupplier()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Degree firstDegree = new Degree() { Id = 10 };
            Degree secondDegree = new Degree() { Id = 20 };

            persistentStore.AddDegree(firstDegree, Supplier);
            persistentStore.AddDegree(secondDegree, Supplier);

            IList<Degree> DegreesExpected = new List<Degree> { firstDegree, secondDegree };
            IList<Degree> DegreesRecieved = persistentStore.GetDegreesForSupplier(Supplier);

            Assert.Equal(DegreesExpected, DegreesRecieved);
        }

        [Fact]
        public void TestModifuDegreesDatabaseConnectionProblem()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Degree Degree = new Degree() { Id = 20, Title = "Degree1" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.ModifyDegree(Degree, Degree));
        }

        [Fact]
        public void TestModifyDegree()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Degree Degree = new Degree() { Id = 20, Title = "Degree1" };
            persistentStore.PersistentStoreContext.Degrees.Add(Degree);

            Degree NewDegree = new Degree() { Id = 20, Title = "Degree2" };

            persistentStore.ModifyDegree(Degree, NewDegree);

            Degree changedDegree = persistentStore.GetDegree(new Degree() { Id = 20 });

            Assert.Equal(NewDegree.Title, changedDegree.Title);
        }

        [Fact]
        public void TestGetDegreeDatabaseConnectionProblem()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Degree Degree = new Degree() { Id = 204 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetDegree(Degree));
        }

        [Fact]
        public void TestGetDegreeNoDegrees()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Degree Degree = new Degree() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetDegree(Degree));
        }

        [Fact]
        public void TestGetDegreeDegreeNotFound()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Degree Degree = new Degree() { Id = 204 };
            persistentStore.PersistentStoreContext.Degrees.Add(Degree);

            Degree DegreeToFind = new Degree() { Id = 10 };
            Assert.Throws<ArgumentException>(() => persistentStore.GetDegree(DegreeToFind));
        }

        [Fact]
        public void TestGetDegree()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Degree Degree = new Degree() { Id = 20 };
            persistentStore.PersistentStoreContext.Degrees.Add(Degree);

            Degree DegreeToFind = new Degree() { Id = 20 };
            Assert.Equal(Degree, persistentStore.GetDegree(DegreeToFind));
        }

        [Fact]
        public void TestRemoveDatabaseConnectionProblem()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Degree Degree = new Degree() { Id = 20 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.RemoveDegree(Degree));
        }

        [Fact]
        public void TestRemoveDegree()
        {
            DegreePersistentStore persistentStore = new DegreePersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Degree Degree = new Degree() { Id = 20 };
            persistentStore.PersistentStoreContext.Degrees.Add(Degree);

            Degree DegreeToFind = new Degree() { Id = 20 };
            Assert.Equal(Degree, persistentStore.GetDegree(DegreeToFind));

            persistentStore.RemoveDegree(new Degree() { Id = 20 });
            Assert.Throws<ArgumentException>(() => persistentStore.GetDegree(Degree));
        }
    }
}
