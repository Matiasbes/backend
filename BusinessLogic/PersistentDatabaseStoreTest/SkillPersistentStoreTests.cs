﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Skills;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class SkillPersistentStoreTests
    {
        private Mock<PersistentStoreContext> GetPersistentStoreContext()

        {
            var mockContext = new Mock<PersistentStoreContext>();

            mockContext.Setup(mock => mock.Skills).Returns(GetDbSetMock<Skill>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);
            mockSet.Setup(mock => mock.Remove(It.IsAny<T>())).Callback<T>(obj => container.Remove(obj));

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        private Mock<PersistentStoreContext> GetPersistentStoreContextDatabaseConnectionProblem()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Skills).Returns(GetDbSetMockDatabaseConnectionProblem<Skill>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMockDatabaseConnectionProblem<Supplier>().Object);


            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMockDatabaseConnectionProblem<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Throws<InvalidOperationException>();
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Throws<InvalidOperationException>();

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Throws<InvalidOperationException>();

            return mockSet;
        }

        [Fact]
        public void TestAddSkillDatabaseConnectionProblem()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Skill Skill = new Skill() { Id = 20 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.AddSkill(Skill, Supplier));
        }


        [Fact]
        public void TestAddSkillSupplierNotExists()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Skill Skill = new Skill() { Id = 20 };

            Assert.Throws<ArgumentException>(() => persistentStore.AddSkill(Skill, Supplier));
        }


        [Fact]
        public void TestAddSkill()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();        
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Skill Skill = new Skill() { Id = 20 };
            persistentStore.AddSkill(Skill, Supplier);

            persistentStore.PersistentStoreContext.Skills.Add(Skill);

            Skill SkillRecieved = persistentStore.GetSkill(Skill);
            Assert.Equal(Skill, SkillRecieved);
        }

        [Fact]
        public void TestGetSkillsDatabaseConnectionProblem()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetSkillsForSupplier(Supplier));
        }

        [Fact]
        public void TestGetSkillsForSupplierWithZeroSkills()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Assert.Equal(new List<Skill>(), persistentStore.GetSkillsForSupplier(Supplier));
        }

        [Fact]
        public void TestGetSkillsForSupplier()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Skill firstSkill = new Skill() { Id = 10 };
            Skill secondSkill = new Skill() { Id = 20 };

            persistentStore.AddSkill(firstSkill, Supplier);
            persistentStore.AddSkill(secondSkill, Supplier);

            IList<Skill> SkillsExpected = new List<Skill> { firstSkill, secondSkill };
            IList<Skill> SkillsRecieved = persistentStore.GetSkillsForSupplier(Supplier);

            Assert.Equal(SkillsExpected, SkillsRecieved);
        }

        [Fact]
        public void TestModifuSkillsDatabaseConnectionProblem()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Skill Skill = new Skill() { Id = 20, Title = "Skill1" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.ModifySkill(Skill, Skill));
        }

        [Fact]
        public void TestModifySkill()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Skill Skill = new Skill() { Id = 20, Title = "Skill1"};
            persistentStore.PersistentStoreContext.Skills.Add(Skill);

            Skill NewSkill = new Skill() { Id = 20, Title = "Skill2"};

            persistentStore.ModifySkill(Skill, NewSkill);

            Skill changedSkill = persistentStore.GetSkill(new Skill() { Id = 20 });

            Assert.Equal(NewSkill.Title, changedSkill.Title);
        }

        [Fact]
        public void TestGetSkillDatabaseConnectionProblem()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Skill Skill = new Skill() { Id = 204 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetSkill(Skill));
        }

        [Fact]
        public void TestGetSkillNoSkills()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Skill Skill = new Skill() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetSkill(Skill));
        }

        [Fact]
        public void TestGetSkillSkillNotFound()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Skill Skill = new Skill() { Id = 204 };
            persistentStore.PersistentStoreContext.Skills.Add(Skill);

            Skill SkillToFind = new Skill() { Id = 10 };
            Assert.Throws<ArgumentException>(() => persistentStore.GetSkill(SkillToFind));
        }

        [Fact]
        public void TestGetSkill()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Skill Skill = new Skill() { Id = 20 };
            persistentStore.PersistentStoreContext.Skills.Add(Skill);

            Skill SkillToFind = new Skill() { Id = 20 };
            Assert.Equal(Skill, persistentStore.GetSkill(SkillToFind));
        }

        [Fact]
        public void TestRemoveDatabaseConnectionProblem()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Skill Skill = new Skill() { Id = 20 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.RemoveSkill(Skill));
        }

        [Fact]
        public void TestRemoveSkill()
        {
            SkillPersistentStore persistentStore = new SkillPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Skill Skill = new Skill() { Id = 20 };
            persistentStore.PersistentStoreContext.Skills.Add(Skill);

            Skill SkillToFind = new Skill() { Id = 20 };
            Assert.Equal(Skill, persistentStore.GetSkill(SkillToFind));

            persistentStore.RemoveSkill(new Skill() { Id = 20 });
            Assert.Throws<ArgumentException>(() => persistentStore.GetSkill(Skill));
        }

    }
}
