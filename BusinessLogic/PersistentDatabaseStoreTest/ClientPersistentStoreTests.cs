﻿using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class ClientPersistentStoreTests
    {

        private Mock<PersistentStoreContext> GetPersistentStoreContext()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Clients).Returns(GetDbSetMock<Client>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        private Mock<PersistentStoreContext> GetPersistentStoreContextDatabaseConnectionProblem()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Clients).Returns(GetDbSetMockDatabaseConnectionProblem<Client>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMockDatabaseConnectionProblem<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Throws<InvalidOperationException>();
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Throws<InvalidOperationException>();

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Throws<InvalidOperationException>();

            return mockSet;
        }

        [Fact]
        public void TestExistsClientWithDatabaseConnectioonProblem()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Client clientToSearch = new Client() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.ExistsClient(clientToSearch));
        }

        [Fact]
        public void TestExistsClientNotExists()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(client);

            Client clientToSearch = new Client() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };

            Assert.False(persistentStore.ExistsClient(clientToSearch));
        }

        [Fact]
        public void TestExistsClient()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(client);

            Assert.True(persistentStore.ExistsClient(client));
        }

        [Fact]
        public void TestGetClientNoClients()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Throws<ArgumentException>(() => persistentStore.GetClient(client));
        }

        [Fact]
        public void TestGetClientWithDatabaseConnectioonProblem()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Client clientToSearch = new Client() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetClient(clientToSearch));
        }

        [Fact]
        public void TestGetClientClientNotFound()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };
            persistentStore.PersistentStoreContext.Clients.Add(client);

            Client clientToSearch = new Client() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };
            Assert.Throws<ArgumentException>(() => persistentStore.GetClient(clientToSearch));
        }

        [Fact]
        public void TestGetClient()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };
            persistentStore.PersistentStoreContext.Clients.Add(client);

            Client clientToSearch = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Equal(client, persistentStore.GetClient(clientToSearch));
        }


        [Fact]
        public void TestModifyClientNoClients()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Name = "Juan", Email = "matias@gmail.com", Password = "contra" };

            Assert.Throws<ArgumentException>(() => persistentStore.ModifyClient(client, client));
        }

        [Fact]
        public void TestModifyClient()
        {
            ClientPersistentStore persistentStore = new ClientPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client client = new Client() { Id = 2, Name = "Juan", Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(client);

            Client NewClient = new Client() { Id = 2, Name = "Fernando", Email = "matias@gmail.com", Password = "contra" };

            persistentStore.ModifyClient(client, NewClient);

            Assert.Equal(NewClient.Name, persistentStore.GetClient(NewClient).Name);
        }

    }
}
