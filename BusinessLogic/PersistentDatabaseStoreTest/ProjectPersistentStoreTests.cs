﻿using BusinessLogic.Projects;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class ProjectPersistentStoreTests
    {
        private Mock<PersistentStoreContext> GetPersistentStoreContext()

        {
            var mockContext = new Mock<PersistentStoreContext>();

            mockContext.Setup(mock => mock.Projects).Returns(GetDbSetMock<Project>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);
            mockContext.Setup(mock => mock.Clients).Returns(GetDbSetMock<Client>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        [Fact]
        public void TestAddProject()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Project Project = new Project() { Id = 20, Client = Client };
            persistentStore.AddProject(Project);

            Project ProjectRecieved = persistentStore.GetProject(Project);
            Assert.Equal(Project, ProjectRecieved);
        }

        [Fact]
        public void TestExistsProjectNoExists()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Assert.False(persistentStore.ExistsProject(new Project { Id = 2 }));
        }

        [Fact]
        public void TestExistsProjectExists()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Project Project = new Project() { Id = 20, Client = Client };
            persistentStore.AddProject(Project);


            Assert.True(persistentStore.ExistsProject(new Project { Id = 20 }));
        }

        [Fact]
        public void TestGetClientProjectsWithNoProjects()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Assert.Equal(new List<Project>(), persistentStore.GetClientProjects(Client));
        }

        [Fact]
        public void TestGetClientProjects()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Project firstProject = new Project() { Id = 10, Client = Client };
            Project secondProject = new Project() { Id = 20, Client = Client };

            persistentStore.AddProject(firstProject);
            persistentStore.AddProject(secondProject);

            IList<Project> ProjectsExpected = new List<Project> { firstProject, secondProject };
            IList<Project> ProjectsRecieved = persistentStore.GetClientProjects(Client);

            Assert.Equal(ProjectsExpected, ProjectsRecieved);
        }

        [Fact]
        public void TestGetProjectNoProjects()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Project Project = new Project() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetProject(Project));
        }

        [Fact]
        public void TestGetProjectProjectNotFound()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Project Project = new Project() { Id = 204 };
            persistentStore.PersistentStoreContext.Projects.Add(Project);

            Project ProjectToFind = new Project() { Id = 10 };
            Assert.Throws<ArgumentException>(() => persistentStore.GetProject(ProjectToFind));
        }

        [Fact]
        public void TestGetProject()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Project Project = new Project() { Id = 20 };
            persistentStore.PersistentStoreContext.Projects.Add(Project);

            Project ProjectToFind = new Project() { Id = 20 };
            Assert.Equal(Project, persistentStore.GetProject(ProjectToFind));
        }

        [Fact]
        public void TestGetSupplierProjects()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Supplier Supplier = new Supplier() { Id = 10, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Project firstProject = new Project() { Id = 10, Client = Client, Suppliers = new List<Supplier> { Supplier } };
            Project secondProject = new Project() { Id = 20, Client = Client, Suppliers = new List<Supplier> { Supplier } };

            persistentStore.AddProject(firstProject);
            persistentStore.AddProject(secondProject);

            IList<Project> ProjectsExpected = new List<Project> { firstProject, secondProject };
            IList<Project> ProjectsRecieved = persistentStore.GetSupplierProjects(Supplier);

            Assert.Equal(ProjectsExpected, ProjectsRecieved);
        }

        [Fact]
        public void TestModifyProject()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Project Project = new Project() { Id = 20, Title = "Project1" };
            persistentStore.PersistentStoreContext.Projects.Add(Project);

            Project NewProject = new Project() { Id = 20, Title = "Project2" };

            persistentStore.ModifyProject(Project, NewProject);

            Project changedProject = persistentStore.GetProject(new Project() { Id = 20 });

            Assert.Equal(NewProject.Title, changedProject.Title);
        }

        [Fact]
        public void TestFinalizeProjectSupplierNotExists()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Supplier Supplier = new Supplier() { Id = 10, Email = "matias@gmail.com", Password = "contra" };

            Project Project = new Project() { Id = 20, Client = Client };
            persistentStore.AddProject(Project);

            ProjectRating rating = new ProjectRating { Id = 10, ClientRate = 4, SupplierRate = 5, Supplier = Supplier };

            Assert.Throws<ArgumentException>(() => persistentStore.FinalizeProject(Project, new List<ProjectRating> { rating }));
        }

        [Fact]
        public void TestFinalizeProject()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Supplier Supplier = new Supplier() { Id = 10, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Project Project = new Project() { Id = 20, Client = Client };
            persistentStore.AddProject(Project);

            ProjectRating rating = new ProjectRating { Id = 10, ClientRate = 4, SupplierRate = 5, Supplier = Supplier };

            persistentStore.FinalizeProject(Project, new List<ProjectRating> {rating});

            Project finalizedProject = persistentStore.GetProject(Project);
            Assert.Equal(finalizedProject.Status, ProjectStatus.Finished);
         }

        [Fact]
        public void TesRateProject()
        {
            ProjectPersistentStore persistentStore = new ProjectPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Client Client = new Client() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Clients.Add(Client);

            Supplier Supplier = new Supplier() { Id = 10, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Project Project = new Project() { Id = 20, Client = Client };
            persistentStore.AddProject(Project);

            ProjectRating rating = new ProjectRating { Id = 10, ClientRate = 4, SupplierRate = 5, Supplier = Supplier };

            persistentStore.FinalizeProject(Project, new List<ProjectRating> { rating });

            persistentStore.RateClient(Supplier, Project, 1);

            Project finalizedProject = persistentStore.GetProject(Project);
            Assert.Equal(finalizedProject.Ratings[0].ClientRate, 1);
        }
    }
}
