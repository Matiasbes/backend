﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Jobs;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class JobPersistentStoreTests
    {
        private Mock<PersistentStoreContext> GetPersistentStoreContext()

        {
            var mockContext = new Mock<PersistentStoreContext>();

            mockContext.Setup(mock => mock.Jobs).Returns(GetDbSetMock<Job>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);
            mockSet.Setup(mock => mock.Remove(It.IsAny<T>())).Callback<T>(obj => container.Remove(obj));

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        private Mock<PersistentStoreContext> GetPersistentStoreContextDatabaseConnectionProblem()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Jobs).Returns(GetDbSetMockDatabaseConnectionProblem<Job>().Object);
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMockDatabaseConnectionProblem<Supplier>().Object);


            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMockDatabaseConnectionProblem<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Throws<InvalidOperationException>();
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Throws<InvalidOperationException>();

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Throws<InvalidOperationException>();

            return mockSet;
        }

        [Fact]
        public void TestAddJobDatabaseConnectionProblem()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Job Job = new Job() { Id = 20 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.AddJob(Job, Supplier));
        }

        [Fact]
        public void TestAddJobSupplierNotExists()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Job Job = new Job() { Id = 20 };

            Assert.Throws<ArgumentException>(() => persistentStore.AddJob(Job, Supplier));
        }


        [Fact]
        public void TestAddJob()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Job Job = new Job() { Id = 20 };
            persistentStore.AddJob(Job, Supplier);

            persistentStore.PersistentStoreContext.Jobs.Add(Job);

            Job JobRecieved = persistentStore.GetJob(Job);
            Assert.True(Supplier.Jobs.Count() == 1);
        }

        [Fact]
        public void TestGetJobsDatabaseConnectionProblem()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetJobsForSupplier(Supplier));
        }

        [Fact]
        public void TestGetJobsForSupplierWithZeroJobs()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Assert.Equal(new List<Job>(), persistentStore.GetJobsForSupplier(Supplier));
        }

        [Fact]
        public void TestGetJobsForSupplier()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Job firstJob = new Job() { Id = 10 };
            Job secondJob = new Job() { Id = 20 };

            persistentStore.AddJob(firstJob, Supplier);
            persistentStore.AddJob(secondJob, Supplier);

            IList<Job> JobsExpected = new List<Job> { firstJob, secondJob };
            IList<Job> JobsRecieved = persistentStore.GetJobsForSupplier(Supplier);

            Assert.Equal(JobsExpected, JobsRecieved);
        }

        [Fact]
        public void TestModifuJobsDatabaseConnectionProblem()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Job Job = new Job() { Id = 20, Title = "Job1" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.ModifyJob(Job, Job));
        }

        [Fact]
        public void TestModifyJob()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Job Job = new Job() { Id = 20, Title = "Job1" };
            persistentStore.PersistentStoreContext.Jobs.Add(Job);

            Job NewJob = new Job() { Id = 20, Title = "Job2" };

            persistentStore.ModifyJob(Job, NewJob);

            Job changedJob = persistentStore.GetJob(new Job() { Id = 20 });

            Assert.Equal(NewJob.Title, changedJob.Title);
        }

        [Fact]
        public void TestGetJobDatabaseConnectionProblem()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Job Job = new Job() { Id = 204 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetJob(Job));
        }

        [Fact]
        public void TestGetJobNoJobs()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Job Job = new Job() { Id = 204 };

            Assert.Throws<ArgumentException>(() => persistentStore.GetJob(Job));
        }

        [Fact]
        public void TestGetJobJobNotFound()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Job Job = new Job() { Id = 204 };
            persistentStore.PersistentStoreContext.Jobs.Add(Job);

            Job JobToFind = new Job() { Id = 10 };
            Assert.Throws<ArgumentException>(() => persistentStore.GetJob(JobToFind));
        }

        [Fact]
        public void TestGetJob()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Job Job = new Job() { Id = 20 };
            persistentStore.PersistentStoreContext.Jobs.Add(Job);

            Job JobToFind = new Job() { Id = 20 };
            Assert.Equal(Job, persistentStore.GetJob(JobToFind));
        }

        [Fact]
        public void TestRemoveDatabaseConnectionProblem()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Job Job = new Job() { Id = 20 };

            Assert.Throws<PersistentStoreException>(() => persistentStore.RemoveJob(Job));
        }

        [Fact]
        public void TestRemoveJob()
        {
            JobPersistentStore persistentStore = new JobPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Job Job = new Job() { Id = 20 };
            persistentStore.PersistentStoreContext.Jobs.Add(Job);

            Job JobToFind = new Job() { Id = 20 };
            Assert.Equal(Job, persistentStore.GetJob(JobToFind));

            persistentStore.RemoveJob(new Job() { Id = 20 });
            Assert.Throws<ArgumentException>(() => persistentStore.GetJob(Job));
        }
    }
}
