﻿using BusinessLogic.Users.Suppliers;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PersistentDatabaseStoreTest
{
    public class SupplierPersistentStoreTests
    {
        private Mock<PersistentStoreContext> GetPersistentStoreContext()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMock<Supplier>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMock<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Callback<T>(obj => container.Add(obj));
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Returns(mockSet.Object);

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Returns((container.AsQueryable()).Provider);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Returns((container.AsQueryable()).Expression);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Returns((container.AsQueryable()).ElementType);
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Returns(container.GetEnumerator());

            return mockSet;
        }

        private Mock<PersistentStoreContext> GetPersistentStoreContextDatabaseConnectionProblem()
        {
            var mockContext = new Mock<PersistentStoreContext>();
            mockContext.Setup(mock => mock.Suppliers).Returns(GetDbSetMockDatabaseConnectionProblem<Supplier>().Object);

            return mockContext;
        }

        private Mock<DbSet<T>> GetDbSetMockDatabaseConnectionProblem<T>() where T : class
        {
            List<T> container = new List<T>();

            var mockSet = new Mock<DbSet<T>>();
            mockSet.Setup(mock => mock.Add(It.IsAny<T>())).Throws<InvalidOperationException>();
            mockSet.Setup(mock => mock.Include(It.IsAny<string>())).Throws<InvalidOperationException>();

            mockSet.As<IQueryable<T>>().Setup(mock => mock.Provider).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.Expression).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.ElementType).Throws<InvalidOperationException>();
            mockSet.As<IQueryable<T>>().Setup(mock => mock.GetEnumerator()).Throws<InvalidOperationException>();

            return mockSet;
        }

        [Fact]
        public void TestExistsSupplierWithDatabaseConnectioonProblem()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier SupplierToSearch = new Supplier() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.ExistsSupplier(SupplierToSearch));
        }

        [Fact]
        public void TestExistsSupplierNotExists()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Supplier SupplierToSearch = new Supplier() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };

            Assert.False(persistentStore.ExistsSupplier(SupplierToSearch));
        }

        [Fact]
        public void TestExistsSupplier()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Assert.True(persistentStore.ExistsSupplier(Supplier));
        }

        [Fact]
        public void TestGetSupplierWithDatabaseConnectioonProblem()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContextDatabaseConnectionProblem().Object;

            Supplier SupplierToSearch = new Supplier() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };

            Assert.Throws<PersistentStoreException>(() => persistentStore.GetSupplier(SupplierToSearch));
        }

        [Fact]
        public void TestGetSupplierNoSuppliers()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Throws<ArgumentException>(() => persistentStore.GetSupplier(Supplier));
        }

        [Fact]
        public void TestGetSupplierSupplierNotFound()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };
            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Supplier SupplierToSearch = new Supplier() { Id = 40, Email = "fercastagno@gmail.com", Password = "Pass123" };
            Assert.Throws<ArgumentException>(() => persistentStore.GetSupplier(SupplierToSearch));
        }

        [Fact]
        public void TestGetSupplier()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };
            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Supplier SupplierToSearch = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra" };

            Assert.Equal(Supplier, persistentStore.GetSupplier(SupplierToSearch));
        }

        [Fact]
        public void TestModifySupplierNoSuppliers()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra", Name = "Matias", Phone = "099123123" };

            Assert.Throws<ArgumentException>(() => persistentStore.ModifySupplier(Supplier, Supplier));
        }

        [Fact]
        public void TestModifySupplier()
        {
            SupplierPersistentStore persistentStore = new SupplierPersistentStore();
            persistentStore.PersistentStoreContext = GetPersistentStoreContext().Object;

            Supplier Supplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra", Name = "Matias", Phone = "099123123"};
            persistentStore.PersistentStoreContext.Suppliers.Add(Supplier);

            Supplier NewSupplier = new Supplier() { Id = 2, Email = "matias@gmail.com", Password = "contra", Name = "Juan", Phone = "099321321" };

            persistentStore.ModifySupplier(Supplier, NewSupplier);

            Supplier changedSupplier = persistentStore.GetSupplier(new Supplier() { Id = 2 });

            Assert.Equal(NewSupplier.Name, changedSupplier.Name);

            Assert.Equal(NewSupplier.Phone, changedSupplier.Phone);
        }
    }
}
