﻿using BusinessLogic.Projects;
using BusinessLogic.Users.Suppliers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class ProjectsControllerTests
    {
        private Mock<ProjectsDataProvider> GetDataProvider()
        {
            List<Project> ProjectsContainer = new List<Project>();
      
            var dataProvider = new Mock<ProjectsDataProvider>();

            dataProvider.Setup(provider => provider.AddProject(It.IsAny<Project>())).Callback<Project>(u => ProjectsContainer.Add(u));
            dataProvider.Setup(provider => provider.GetProject(It.Is<Project>(u => ProjectsContainer.Contains(u))))
                .Returns((Project u) => ProjectsContainer.First(u.Equals));
            dataProvider.Setup(provider => provider.ExistsProject(It.IsAny<Project>())).Returns<Project>((ProjectToCheck) => ExistsProject(ProjectsContainer, ProjectToCheck));
            return dataProvider;
        }

        private bool ExistsProject(List<Project> Projects, Project Project)
        {
            foreach (Project u in Projects)
            {
                if (u.Id == Project.Id)
                    return true;
            }
            return false;
        }

        [Fact]
        public void TestAddNullProject()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.AddProject(null));
        }

        [Fact]
        public void TestAddProjectWithoutTitle()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Project Project = new Project();
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }

        [Fact]
        public void TestAddProjectWithoutDescription()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Project Project = new Project() { Title = "Proj" };
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }

        [Fact]
        public void TestAddProjectLowBudget()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = -1 };
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }

        [Fact]
        public void TestAddProjectNoDeadline()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1 };
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }

        [Fact]
        public void TestAddProjectPastDeadline()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Project Project = new Project() { Title = "Proj", Status = ProjectStatus.Pending, Description = "Desc", Budget = 1, Deadline = new DateTime(2015,5,5) };
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }

        [Fact]
        public void TestAddProjectNoSuppliers()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Status = ProjectStatus.InProcess };
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }

        [Fact]
        public void TestAddProject()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            IList<Supplier> Suppliers= new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            Suppliers.Add(sup);
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Suppliers = Suppliers };
            controller.AddProject(Project);
            Assert.Equal(Project.Title, controller.GetProject(Project).Title);
        }

        [Fact]
        public void TestAddProjectAlreadyExists()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            IList<Supplier> Suppliers = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            Suppliers.Add(sup);
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Suppliers = Suppliers };
            controller.AddProject(Project);
            Assert.Throws<ArgumentException>(() => controller.AddProject(Project));
        }


        [Fact]
        public void TestGetNullProject()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetProject(null));
        }

        [Fact]
        public void TestGetProjectNotExists()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            IList<Supplier> Suppliers = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            Suppliers.Add(sup);
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Suppliers = Suppliers };
            Assert.Throws<ArgumentException>(() => controller.GetProject(Project));
        }

        [Fact]
        public void TestModifyNullProject()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.ModifyProject(null, null));
        }

        [Fact]
        public void TestModifyProjectNotExists()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            IList<Supplier> Suppliers = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            Suppliers.Add(sup);
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Suppliers = Suppliers };
            Assert.Throws<ArgumentException>(() => controller.ModifyProject(Project, Project));
        }

        [Fact]
        public void TestGetNullClientProjects()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetClientProjects(null));
        }

        [Fact]
        public void TestGetNullSupplierProjects()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetSupplierProjects(null));
        }

        [Fact]
        public void TestRateClientNullSupplier()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.RateClient(null, null, 3));
        }

        [Fact]
        public void TestRateClientProjectNotFinalized()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            IList<Supplier> Suppliers = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            Suppliers.Add(sup);
            Project Project = new Project() { Title = "Proj", Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Suppliers = Suppliers };
            controller.AddProject(Project);
            Assert.Throws<ArgumentException>(() => controller.RateClient(sup, Project, 3));
        }

        [Fact]
        public void TestRateClientWrongSupplier()
        {
            var provider = GetDataProvider().Object;
            ProjectsController controller = new ProjectsController();
            controller.DataProvider = provider;
            IList<Supplier> Suppliers = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            Supplier WrongSup = new Supplier() { Id = 2 };
            Suppliers.Add(sup);
            Project Project = new Project() { Title = "Proj", Status = ProjectStatus.Finished, Description = "Desc", Budget = 1, Deadline = new DateTime(2018, 5, 5), Suppliers = Suppliers };
            controller.AddProject(Project);
            Assert.Throws<ArgumentException>(() => controller.RateClient(WrongSup, Project, 3));
        }

    }
}
