﻿using BusinessLogic.Users.Suppliers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class SuppliersCotrollerTests
    {

        private Mock<SuppliersDataProvider> GetDataProvider()
        {
            List<Supplier> SupplierContainer = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com", Overview = "Over", Rate = 4 };
            SupplierContainer.Add(sup);
            var dataProvider = new Mock<SuppliersDataProvider>();
            dataProvider.Setup(provider => provider.GetSupplier(It.IsAny<Supplier>())).Returns(SupplierContainer.First());
            dataProvider.Setup(provider => provider.ExistsSupplier(It.IsAny<Supplier>())).Returns<Supplier>((SupplierToCheck) => ExistsSupplier(SupplierContainer, SupplierToCheck));
            dataProvider.Setup(provider => provider.ModifySupplier(It.IsAny<Supplier>(), It.IsAny<Supplier>())).Callback<Supplier, Supplier>((SupplierToModify, newSupplier) => ModifySupplier(SupplierContainer, SupplierToModify, newSupplier));
            return dataProvider;
        }

        private Supplier ModifySupplier(List<Supplier> Suppliers, Supplier SupplierToModify, Supplier newSupplier)
        {
            Supplier realSupplierToModify = Suppliers.First();
            realSupplierToModify.Name = newSupplier.Name;
            realSupplierToModify.Overview = newSupplier.Overview;
            realSupplierToModify.Rate = newSupplier.Rate;
            realSupplierToModify.Phone = newSupplier.Phone;
            return realSupplierToModify;
        }

        private bool ExistsSupplier(List<Supplier> Suppliers, Supplier Supplier)
        {
            Supplier existingSupplier = Suppliers.First();
            if (existingSupplier.Id == Supplier.Id)
                return true;
            return false;
        }


        [Fact]
        public void TestGetNullSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetSupplier(null));
        }

        [Fact]
        public void TestGetNonExistingSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.GetSupplier(sup));
        }

        [Fact]
        public void TestGetSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier supplierToGet = new Supplier() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com", Overview = "Over", Rate = 4 };
            Assert.Equal(supplierToGet.Name, controller.GetSupplier(supplierToGet).Name);
            Assert.Equal(supplierToGet.Email, controller.GetSupplier(supplierToGet).Email);
            Assert.Equal(supplierToGet.Overview, controller.GetSupplier(supplierToGet).Overview);
        }

        [Fact]
        public void TestModifySupplierNullToModify()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.ModifySupplier(null, sup));
        }

        [Fact]
        public void TestModifySupplierNullNewSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.ModifySupplier(sup, null));
        }

        [Fact]
        public void TestModifySupplierNoNameNewSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.ModifySupplier(sup, sup));
        }

        [Fact]
        public void TestModifySupplierNoDescriptionNewSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1, Name = "Fernando" };
            Assert.Throws<ArgumentException>(() => controller.ModifySupplier(sup, sup));
        }

        [Fact]
        public void TestModifySupplierZeroRateNewSupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1, Name = "Fernando", Overview = "over", Rate = -1 };
            Assert.Throws<ArgumentException>(() => controller.ModifySupplier(sup, sup));
        }

        [Fact]
        public void TestModifySupplierNotExists()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1, Name = "Fernando", Overview = "over", Rate = 0 };
            Assert.Throws<ArgumentException>(() => controller.ModifySupplier(sup, sup));
        }

        [Fact]
        public void TestModifySupplier()
        {
            var provider = GetDataProvider().Object;
            SuppliersController controller = new SuppliersController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com", Overview = "Over", Rate = 4 };
            Supplier sup2 = new Supplier() { Id = 100, Name = "Juan", Email = "fercastagno@gmail.com", Overview = "Over", Rate = 4 };
            controller.ModifySupplier(sup, sup2);
            Assert.Equal(sup2.Name, controller.GetSupplier(sup2).Name);
        }
    }
}
