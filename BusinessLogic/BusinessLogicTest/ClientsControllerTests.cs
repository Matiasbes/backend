﻿using BusinessLogic.Users.Clients;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class ClientsControllerTests
    {
        private Mock<ClientsDataProvider> GetDataProvider()
        {
            List<Client> ClientContainer = new List<Client>();
            Client cli = new Client() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com", Phone = "099322548" };
            ClientContainer.Add(cli);
            var dataProvider = new Mock<ClientsDataProvider>();
            dataProvider.Setup(provider => provider.GetClient(It.IsAny<Client>())).Returns(ClientContainer.First());
            dataProvider.Setup(provider => provider.ExistsClient(It.IsAny<Client>())).Returns<Client>((ClientToCheck) => ExistsClient(ClientContainer, ClientToCheck));
            dataProvider.Setup(provider => provider.ModifyClient(It.IsAny<Client>(), It.IsAny<Client>()))
               .Callback<Client, Client>((ClientToModify, newClient) => ModifyClient(ClientContainer, ClientToModify, newClient));
            return dataProvider;
        }

        private Client ModifyClient(List<Client> Clients, Client ClientToModify, Client newClient)
        {
            Client realClientToModify = Clients.First();
            realClientToModify.Name = newClient.Name;
            realClientToModify.Phone = newClient.Phone;
            return realClientToModify;
        }

        private bool ExistsClient(List<Client> Clients, Client Client)
        {
            Client existingClient = Clients.First();
            if (existingClient.Id == Client.Id)
                return true;
            return false;
        }


        [Fact]
        public void TestGetNullClient()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetClient(null));
        }

        [Fact]
        public void TestGetNonExistingClient()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.GetClient(cli));
        }

        [Fact]
        public void TestGetClient()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client ClientToGet = new Client() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com", Phone = "099322548" };
            Assert.Equal(ClientToGet.Name, controller.GetClient(ClientToGet).Name);
            Assert.Equal(ClientToGet.Email, controller.GetClient(ClientToGet).Email);
            Assert.Equal(ClientToGet.Phone, controller.GetClient(ClientToGet).Phone);
        }

        [Fact]
        public void TestModifyClientNullToModify()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 100 };
            Assert.Throws<ArgumentException>(() => controller.ModifyClient(null, cli));
        }

        [Fact]
        public void TestModifyClientNullNewClient()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 100 };
            Assert.Throws<ArgumentException>(() => controller.ModifyClient(cli, null));
        }

        [Fact]
        public void TestModifyClientNoNameNewClient()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 100};
            
            Assert.Throws<ArgumentException>(() => controller.ModifyClient(cli, cli));
        }

        [Fact]
        public void TestModifyClientNotExists()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 50, Name = "Fernando", Phone = "099322548" };
            Assert.Throws<ArgumentException>(() => controller.ModifyClient(cli, cli));
        }

        [Fact]
        public void TestModifyClient()
        {
            var provider = GetDataProvider().Object;
            ClientsController controller = new ClientsController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com", Phone = "099322548" };
            Client cli2 = new Client() { Id = 100, Name = "Juan", Email = "fercastagno@gmail.com", Phone = "099322548" };
            controller.ModifyClient(cli, cli2);
            Assert.Equal(cli2.Name, controller.GetClient(cli2).Name);
        }
    }
}
