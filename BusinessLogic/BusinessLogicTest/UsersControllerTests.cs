﻿using BusinessLogic.Authentication;
using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using Moq;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class UserControllerTests
    {
        private Mock<UsersDataProvider> GetDataProvider()
        {
            List<User> UsersContainer = new List<User>();
            List<Client> ClientsContainer = new List<Client>();
            Client cli = new Client() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com" };
            ClientsContainer.Add(cli);
            List<Supplier> SupplierContainer = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 200, Name = "Juan", Email = "juan@gmail.com" };
            SupplierContainer.Add(sup);
            List<Session> SessionCotainer = new List<Session>();
            Session sec = new Session() { Id = 10 };
            SessionCotainer.Add(sec);
            var dataProvider = new Mock<UsersDataProvider>();
            dataProvider.Setup(provider => provider.AddUser(It.IsAny<User>())).Callback<User>(u => UsersContainer.Add(u));
            dataProvider.Setup(provider => provider.GetUser(It.Is<User>(u => !UsersContainer.Contains(u)))).Throws<ArgumentException>();
            dataProvider.Setup(provider => provider.GetClientFromUser(It.IsAny<User>())).Returns(ClientsContainer.First());
            dataProvider.Setup(provider => provider.GetSupplierFromUser(It.IsAny<User>())).Returns(SupplierContainer.First());
            dataProvider.Setup(provider => provider.GetUser(It.Is<User>(u => UsersContainer.Contains(u)))).Returns((User u) => UsersContainer.First());
            dataProvider.Setup(provider => provider.ExistsUserWithEmail(It.IsAny<string>())).Returns<string>((UserToCheck) => ExistsUserWithEmail(UsersContainer, UserToCheck));
            dataProvider.Setup(provider => provider.GetUserWithCredentials(It.IsAny<string>(), It.IsAny<string>())).Returns<string, string>((email, pass) => GetUserWithCredentials(UsersContainer, email, pass));
            dataProvider.Setup(provider => provider.GetUserWithFacebookId(It.IsAny<string>())).Returns<string>((facebook) => GetUserWithFacebookId(UsersContainer, facebook));
            dataProvider.Setup(provider => provider.ChangePassword(It.IsAny<User>(), It.IsAny<string>())).Returns<User,string>((UserToCheck, NewPassword) => ChangePassword(UsersContainer, UserToCheck, NewPassword));
            dataProvider.Setup(provider => provider.RemoveSession(It.IsAny<Session>())).Callback<Session>(s => SessionCotainer.Remove(s));
            dataProvider.Setup(provider => provider.AddSessionToUser(It.IsAny<User>(), It.IsAny<Session>())).Callback<User, Session>((UserToCheck, NewPassword) => AddSessionToUser(UsersContainer, UserToCheck, NewPassword));
            return dataProvider;
        }

        private User GetUserWithCredentials(List<User> Users, string email, string pass)
        {
            foreach (User u in Users)
            {
                if (u.Email == email && u.Password == pass)
                {
                    return u;
                }
            }
            return null;
        }

        private Session AddSessionToUser(List<User> Users, User user, Session sec)
        {
            foreach (User u in Users)
            {
                if (u.Id == user.Id)
                {
                    u.Sessions.Add(sec);
                    return sec;
                }
            }
            return null;
        }

        private User ChangePassword(List<User> Users, User user, string password)
        {
            foreach (User u in Users)
            {
                if (u.Id == user.Id)
                {
                    u.Password = password;
                    return u;
                }
            }
            return null;
        }

        private string GeneratePasswordHash(string password)
        {
            using (var sha = new MD5CryptoServiceProvider())
            {
                var passwordBytes = Encoding.UTF8.GetBytes(password);
                return string.Concat(sha.ComputeHash(passwordBytes).Select(x => x.ToString("X2")));
            }
        }

        private bool ExistsUserWithEmail(List<User> Users, string Email)
        {
            foreach(User u in Users)
            {
                if (u.Email == Email)
                    return true;
            }
            return false;
        }

        private User GetUserWithFacebookId(List<User> Users, string FacebookId)
        {
            foreach (User u in Users)
            {
                if (u.FacebookId == FacebookId)
                    return u;
            }
            return null;
        }


        [Fact]
        public void TestRemoveSession()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Session sec = new Session() { Id = 10};
            controller.RemoveSession(sec);
        }

        [Fact]
        public void TestAddNullUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.AddUser(null));
        }

        [Fact]
        public void TestAddUserWithoutName()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User();
            Assert.Throws<ArgumentException>(() => controller.AddUser(user));
        }

        [Fact]
        public void TestAddUserWithoutEmail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Name = "Fernando" };
            Assert.Throws<ArgumentException>(() => controller.AddUser(user));
        }

        [Fact]
        public void TestAddUserWithoutPassword()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Name = "Fernando", Email = "fercastagno@gmail.com" };
            Assert.Throws<ArgumentException>(() => controller.AddUser(user));
        }

        [Fact]
        public void TestAddUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123"};
            controller.AddUser(user);
            User addedUser = controller.GetUser(user);
            Assert.Equal(user.Name, addedUser.Name);
            Assert.Equal(user.Email, addedUser.Email);
        }

        [Fact]
        public void TestAddUserWithExistingEmail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            controller.AddUser(user);
            Assert.Throws<ArgumentException>(() => controller.AddUser(user));
        }

        [Fact]
        public void TestExistsUserWithInvalidEmail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            controller.AddUser(user);
            Assert.Throws<ArgumentException>(() => controller.ExistsUserWithEmail("asdf"));
        }

        [Fact]
        public void TestExistsUserWithExistingEmail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            controller.AddUser(user);
            Assert.True(controller.ExistsUserWithEmail("fercastagno@gmail.com"));
        }

        [Fact]
        public void TestExistsUserWithNonExistingEmail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.False(controller.ExistsUserWithEmail("fercastagno@gmail.com"));
        }

        [Fact]
        public void TesGetUserWithFacebookIdNull()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetUserWithFacebookId(null));
        }

        [Fact]
        public void TesGetUserWithFacebookId()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123", FacebookId = "facebook" };
            controller.AddUser(user);
            Assert.Equal(user.Name, controller.GetUserWithFacebookId("facebook").Name);
            Assert.Equal(user.Email, controller.GetUserWithFacebookId("facebook").Email);
        }

        [Fact]
        public void TestNewSessionForUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123", FacebookId = "facebook" };
            controller.AddUser(user);
            controller.NewSessionForUser(user);
            Assert.True(controller.GetUser(user).Sessions.Count > 0);
        }

        [Fact]
        public void TestGetNullUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetUser(null));
        }

        [Fact]
        public void TestGetUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            controller.AddUser(user);
            Assert.Equal(user.Name, controller.GetUser(user).Name);
        }

        [Fact]
        public void TestGetClientFromUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Client cli = new Client() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com" };
            User user = new User() { Id = 100, Name = "Fernando", Email = "fercastagno@gmail.com"};
            Assert.Equal(cli.Name, controller.GetClientFromUser(user).Name);
            Assert.Equal(cli.Email, controller.GetClientFromUser(user).Email);
        }

        [Fact]
        public void TestGetSupplierFromUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Supplier cli = new Supplier() { Id = 50, Name = "Juan", Email = "juan@gmail.com" };
            User user = new User() { Id = 50, Name = "Juan", Email = "juan@gmail.com" };
            Assert.Equal(cli.Name, controller.GetSupplierFromUser(user).Name);
            Assert.Equal(cli.Email, controller.GetSupplierFromUser(user).Email);
        }


        [Fact]

        public void TestGetUserWithCredentialsInvalidMail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetUserWithCredentials("asd", "Pass123"));
        }

        [Fact]
        public void TestGetUserWithCredentialsNoPassword()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetUserWithCredentials("fercastagno@gmail.com", ""));
        }

        [Fact]
        public void TestGetUserWithCredentials()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123"};
            controller.AddUser(user);
            Assert.Equal(user.Name, controller.GetUserWithCredentials("fercastagno@gmail.com", "Pass123").Name);
        }

        [Fact]
        public void TestGeneratePasswordHash()
        {
            string hashpassword;
            string password = "Pass123";
            using (var sha = new MD5CryptoServiceProvider())
            {
                var passwordBytes = Encoding.UTF8.GetBytes(password);
                hashpassword = string.Concat(sha.ComputeHash(passwordBytes).Select(x => x.ToString("X2")));
            }
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Equal(hashpassword, controller.GeneratePasswordHash(password));
        }

        [Fact]
        public void TestChangePasswordNullUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.ChangePassword(null, "Pass123", "Pass321"));
        }

        [Fact]
        public void TestChangePasswordEmptyNewPassword()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            Assert.Throws<ArgumentException>(() => controller.ChangePassword(user, "Pass123", ""));
        }

        [Fact]
        public void TestChangePasswordWrongPassword()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            controller.AddUser(user);
            Assert.Throws<ArgumentException>(() => controller.ChangePassword(user, "Pass321", "Pass654"));
        }

        [Fact]
        public void TestChangePassword()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            controller.AddUser(user);
            controller.ChangePassword(user, "Pass123", "Pass654");
            User passwordChanged = controller.GetUser(user);
            Assert.Equal(passwordChanged.Password, GeneratePasswordHash("Pass654"));
        }

        [Fact]
        public void TestAddNullFacebookUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.AddUserSyncedWithFacebook(null));
        }

        [Fact]
        public void TestAddFacebookUserWithoutName()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User();
            Assert.Throws<ArgumentException>(() => controller.AddUserSyncedWithFacebook(user));
        }

        [Fact]
        public void TestAddFacebookUserWithoutEmail()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Name = "Fernando" };
            Assert.Throws<ArgumentException>(() => controller.AddUserSyncedWithFacebook(user));
        }

        [Fact]
        public void TestAddFacebookUserWithoutPassword()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Name = "Fernando", Email = "fercastagno@gmail.com" };
            Assert.Throws<ArgumentException>(() => controller.AddUserSyncedWithFacebook(user));
        }

        [Fact]
        public void TestAddFacebookUserWithoutFacebookId()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123" };
            Assert.Throws<ArgumentException>(() => controller.AddUserSyncedWithFacebook(user));
        }

        [Fact]
        public void TestAddFacebookUser()
        {
            var provider = GetDataProvider().Object;
            UsersController controller = new UsersController();
            controller.DataProvider = provider;
            User user = new User() { Id = 1, Name = "Fernando", Email = "fercastagno@gmail.com", Password = "Pass123", FacebookId = "123" };
            controller.AddUser(user);
            User addedUser = controller.GetUser(user);
            Assert.Equal(user.Name, addedUser.Name);
            Assert.Equal(user.Email, addedUser.Email);
        }
    }
}
