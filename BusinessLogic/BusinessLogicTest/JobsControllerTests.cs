﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Jobs;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class JobsControllerTests
    {

        private Mock<JobsDataProvider> GetDataProvider()
        {
            List<Job> JobContainer = new List<Job>();
            List<Supplier> SupplierContainer = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            SupplierContainer.Add(sup);
            var dataProvider = new Mock<JobsDataProvider>();
            dataProvider.Setup(provider => provider.GetJob(It.Is<Job>(u => JobContainer.Contains(u))))
                .Returns((Job u) => JobContainer.First(u.Equals));
            dataProvider.Setup(provider => provider.AddJob(It.IsAny<Job>(), It.IsAny<Supplier>())).Callback<Job, Supplier>((job, suppToAdd) => AddJob(JobContainer, SupplierContainer, suppToAdd, job));
            dataProvider.Setup(provider => provider.ModifyJob(It.IsAny<Job>(), It.IsAny<Job>()))
               .Callback<Job, Job>((JobToModify, newJob) => ModifyJob(JobContainer, JobToModify, newJob));
            dataProvider.Setup(provider => provider.RemoveJob(It.IsAny<Job>())).Callback<Job>(d => JobContainer.Remove(d));
            dataProvider.Setup(provider => provider.GetJobsForSupplier(It.IsAny<Supplier>())).Returns<Supplier>((supplierToCheck) => GetJobsForSupplier(SupplierContainer, supplierToCheck));
            return dataProvider;
        }

        private List<Job> GetJobsForSupplier(List<Supplier> Suppliers, Supplier sup)
        {
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    return s.Jobs.ToList();
                }
            }
            return null;
        }

        private void AddJob(List<Job> Jobs, List<Supplier> Suppliers, Supplier sup, Job job)
        {
            Jobs.Add(job);
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    s.Jobs.Add(job);
                }
            }
        }

        private Job ModifyJob(List<Job> Jobs, Job JobToModify, Job newJob)
        {
            Job realJobToModify = Jobs.First();
            realJobToModify.Title = newJob.Title;
            realJobToModify.Description = newJob.Description;
            realJobToModify.Enterprise = newJob.Enterprise;
            return realJobToModify;
        }

        [Fact]
        public void TestAddJobNullJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.AddJob(null, sup));
        }

        [Fact]
        public void TestAddJobNullEnterprise()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10 };
            Assert.Throws<ArgumentException>(() => controller.AddJob(job, sup));
        }

        [Fact]
        public void TestAddJobWrongDate()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Enterprise = "ORT", StartDate = new DateTime(2017, 6, 6), EndDate = new DateTime(2017, 5, 5) };
            Assert.Throws<ArgumentException>(() => controller.AddJob(job, sup));
        }

        [Fact]
        public void TestAddJobFutureDate()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Enterprise = "ORT", StartDate = new DateTime(2018, 6, 6), EndDate = new DateTime(2018, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddJob(job, sup));
        }

        [Fact]
        public void TestAddJobNoTitle()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddJob(job, sup));
        }

        [Fact]
        public void TestAddJobNullSupplier()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Title = "Ing Sis", Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddJob(job, null));
        }

        [Fact]
        public void TestAddJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Title = "Ing Sis", Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddJob(job, sup);
            Assert.Equal(job.Title, controller.GetJob(job).Title);
        }

        [Fact]
        public void TestGetNullJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetJob(null));
        }

        [Fact]
        public void TestGetJobsFromSupplierNullSupplier()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetJobsForSupplier(null));
        }

        [Fact]
        public void TestGetJobsFromSupplier()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Title = "Ing Sis", Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddJob(job, sup);
            IList<Job> Jobs = controller.GetJobsForSupplier(sup);
            Assert.Equal(Jobs.First(), job);
        }

        [Fact]
        public void TestRemoveJobNullJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.RemoveJob(null));
        }

        [Fact]
        public void TestRemoveJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Title = "Ing Sis", Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddJob(job, sup);
            Assert.Equal(job.Title, controller.GetJob(job).Title);
            controller.RemoveJob(job);
            Assert.Null(controller.GetJob(job));
        }

        [Fact]
        public void TestModifyJobNullJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.ModifyJob(null, null));
        }

        [Fact]
        public void TestModifyJob()
        {
            var provider = GetDataProvider().Object;
            JobsController controller = new JobsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Job job = new Job() { Id = 10, Title = "Ing Sis", Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddJob(job, sup);
            Job modifiedjob = new Job() { Id = 10, Title = "Modified Ing Sis", Enterprise = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.ModifyJob(job, modifiedjob);
            Assert.Equal(modifiedjob.Title, controller.GetJob(job).Title);
        }
    }
}
