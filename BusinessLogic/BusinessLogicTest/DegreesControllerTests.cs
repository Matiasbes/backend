﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Degrees;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class DegreesControllerTests
    {

        private Mock<DegreesDataProvider> GetDataProvider()
        {
            List<Degree> DegreeContainer = new List<Degree>();
            List<Supplier> SupplierContainer = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            SupplierContainer.Add(sup);
            var dataProvider = new Mock<DegreesDataProvider>();
            dataProvider.Setup(provider => provider.GetDegree(It.Is<Degree>(u => DegreeContainer.Contains(u))))
                .Returns((Degree u) => DegreeContainer.First(u.Equals));
            dataProvider.Setup(provider => provider.AddDegree(It.IsAny<Degree>(), It.IsAny<Supplier>())).Callback<Degree, Supplier>((deg, suppToAdd) => AddDegree(DegreeContainer, SupplierContainer, suppToAdd, deg));
            dataProvider.Setup(provider => provider.ModifyDegree(It.IsAny<Degree>(), It.IsAny<Degree>()))
               .Callback<Degree, Degree>((DegreeToModify, newDegree) => ModifyDegree(DegreeContainer, DegreeToModify, newDegree));
            dataProvider.Setup(provider => provider.RemoveDegree(It.IsAny<Degree>())).Callback<Degree>(d => DegreeContainer.Remove(d));
            dataProvider.Setup(provider => provider.GetDegreesForSupplier(It.IsAny<Supplier>())).Returns<Supplier>((supplierToCheck) => GetDegreesForSupplier(SupplierContainer, supplierToCheck));
            return dataProvider;
        }

        private List<Degree> GetDegreesForSupplier(List<Supplier> Suppliers, Supplier sup)
        {
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    return s.Degrees.ToList();
                }
            }
            return null;
        }

        private void AddDegree(List<Degree> Degrees, List<Supplier> Suppliers, Supplier sup, Degree deg)
        {
            Degrees.Add(deg);
            foreach(Supplier s in Suppliers)
            {
                if(s.Id == sup.Id)
                {
                    s.Degrees.Add(deg);
                }
            }
        }

        private Degree ModifyDegree(List<Degree> Degrees, Degree DegreeToModify, Degree newDegree)
        {
            Degree realDegreeToModify = Degrees.First();
            realDegreeToModify.Title = newDegree.Title;
            realDegreeToModify.Description = newDegree.Description;
            realDegreeToModify.Institution = newDegree.Institution;
            return realDegreeToModify;
        }

        [Fact]
        public void TestAddDegreeNullDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.AddDegree(null, sup));
        }

        [Fact]
        public void TestAddDegreeNullInstitution()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10 };
            Assert.Throws<ArgumentException>(() => controller.AddDegree(deg, sup));
        }

        [Fact]
        public void TestAddDegreeWrongDate()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Institution = "ORT",  StartDate = new DateTime(2017,6,6), EndDate = new DateTime(2017, 5, 5) };
            Assert.Throws<ArgumentException>(() => controller.AddDegree(deg, sup));
        }

        [Fact]
        public void TestAddDegreeFutureDate()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Institution = "ORT", StartDate = new DateTime(2018, 6, 6), EndDate = new DateTime(2018, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddDegree(deg, sup));
        }

        [Fact]
        public void TestAddDegreeNoTitle()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddDegree(deg, sup));
        }

        [Fact]
        public void TestAddDegreeNullSupplier()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Title = "Ing Sis", Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddDegree(deg, null));
        }

        [Fact]
        public void TestAddDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Title = "Ing Sis", Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddDegree(deg, sup);
            Assert.Equal(deg.Title, controller.GetDegree(deg).Title);
        }

        [Fact]
        public void TestGetNullDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetDegree(null));
        }

        [Fact]
        public void TestGetDegreesFromSupplierNullSupplier()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetDegreesForSupplier(null));
        }

        [Fact]
        public void TestGetDegreesFromSupplier()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Title = "Ing Sis", Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddDegree(deg, sup);
            IList<Degree> degrees = controller.GetDegreesForSupplier(sup);
            Assert.Equal(degrees.First(), deg);
        }

        [Fact]
        public void TestRemoveDegreeNullDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.RemoveDegree(null));
        }

        [Fact]
        public void TestRemoveDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Title = "Ing Sis", Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddDegree(deg, sup);
            Assert.Equal(deg.Title, controller.GetDegree(deg).Title);
            controller.RemoveDegree(deg);
            Assert.Null(controller.GetDegree(deg));
        }

        [Fact]
        public void TestModifyDegreeNullDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.ModifyDegree(null, null));
        }

        [Fact]
        public void TestModifyDegree()
        {
            var provider = GetDataProvider().Object;
            DegreesController controller = new DegreesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Degree deg = new Degree() { Id = 10, Title = "Ing Sis", Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.AddDegree(deg,sup);
            Degree modifiedDeg = new Degree() { Id = 10, Title = "Modified Ing Sis", Institution = "ORT", StartDate = new DateTime(2016, 6, 6), EndDate = new DateTime(2016, 7, 7) };
            controller.ModifyDegree(deg, modifiedDeg);
            Assert.Equal(modifiedDeg.Title, controller.GetDegree(deg).Title);
        }
    }
}
