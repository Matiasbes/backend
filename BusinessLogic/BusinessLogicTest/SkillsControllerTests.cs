﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Skills;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class SkillsControllerTests
    {

        private Mock<SkillsDataProvider> GetDataProvider()
        {
            List<Skill> SkillContainer = new List<Skill>();
            List<Supplier> SupplierContainer = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            SupplierContainer.Add(sup);
            var dataProvider = new Mock<SkillsDataProvider>();
            dataProvider.Setup(provider => provider.GetSkill(It.Is<Skill>(u => SkillContainer.Contains(u))))
                .Returns((Skill u) => SkillContainer.First(u.Equals));
            dataProvider.Setup(provider => provider.AddSkill(It.IsAny<Skill>(), It.IsAny<Supplier>())).Callback<Skill, Supplier>((ski, suppToAdd) => AddSkill(SkillContainer, SupplierContainer, suppToAdd, ski));
            dataProvider.Setup(provider => provider.ModifySkill(It.IsAny<Skill>(), It.IsAny<Skill>()))
               .Callback<Skill, Skill>((SkillToModify, newSkill) => ModifySkill(SkillContainer, SkillToModify, newSkill));
            dataProvider.Setup(provider => provider.RemoveSkill(It.IsAny<Skill>())).Callback<Skill>(d => SkillContainer.Remove(d));
            dataProvider.Setup(provider => provider.GetSkillsForSupplier(It.IsAny<Supplier>())).Returns<Supplier>((supplierToCheck) => GetSkillsForSupplier(SupplierContainer, supplierToCheck));
            return dataProvider;
        }

        private List<Skill> GetSkillsForSupplier(List<Supplier> Suppliers, Supplier sup)
        {
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    return s.Skills.ToList();
                }
            }
            return null;
        }

        private void AddSkill(List<Skill> Skills, List<Supplier> Suppliers, Supplier sup, Skill ski)
        {
            Skills.Add(ski);
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    s.Skills.Add(ski);
                }
            }
        }

        private Skill ModifySkill(List<Skill> Skills, Skill SkillToModify, Skill newSkill)
        {
            Skill realSkillToModify = Skills.First();
            realSkillToModify.Title = newSkill.Title;
            return realSkillToModify;
        }

        [Fact]
        public void TestAddSkillNullSkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.AddSkill(null, sup));
        }


        [Fact]
        public void TestAddSkillNoTitle()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Skill ski = new Skill() { Id = 10 };
            Assert.Throws<ArgumentException>(() => controller.AddSkill(ski, sup));
        }

        [Fact]
        public void TestAddSkillNullSupplier()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Skill ski = new Skill() { Id = 10, Title = "Ing Sis"};
            Assert.Throws<ArgumentException>(() => controller.AddSkill(ski, null));
        }

        [Fact]
        public void TestAddSkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Skill ski = new Skill() { Id = 10, Title = "Ing Sis" };
            controller.AddSkill(ski, sup);
            Assert.Equal(ski.Title, controller.GetSkill(ski).Title);
        }

        [Fact]
        public void TestGetNullSkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetSkill(null));
        }

        [Fact]
        public void TestGetSkillsFromSupplierNullSupplier()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetSkillsForSupplier(null));
        }

        [Fact]
        public void TestGetSkillsFromSupplier()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Skill ski = new Skill() { Id = 10, Title = "Ing Sis"};
            controller.AddSkill(ski, sup);
            IList<Skill> Skills = controller.GetSkillsForSupplier(sup);
            Assert.Equal(Skills.First(), ski);
        }

        [Fact]
        public void TestRemoveSkillNullSkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.RemoveSkill(null));
        }

        [Fact]
        public void TestRemoveSkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Skill ski = new Skill() { Id = 10, Title = "Ing Sis"};
            controller.AddSkill(ski, sup);
            Assert.Equal(ski.Title, controller.GetSkill(ski).Title);
            controller.RemoveSkill(ski);
            Assert.Null(controller.GetSkill(ski));
        }

        [Fact]
        public void TestModifySkillNullSkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.ModifySkill(null, null));
        }

        [Fact]
        public void TestModifySkill()
        {
            var provider = GetDataProvider().Object;
            SkillsController controller = new SkillsController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Skill ski = new Skill() { Id = 10, Title = "Ing Sis"};
            controller.AddSkill(ski, sup);
            Skill modifiedski = new Skill() { Id = 10, Title = "Modified Ing Sis"};
            controller.ModifySkill(ski, modifiedski);
            Assert.Equal(modifiedski.Title, controller.GetSkill(ski).Title);
        }
    }
}
