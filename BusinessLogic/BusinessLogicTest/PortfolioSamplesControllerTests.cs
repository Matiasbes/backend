﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Portfolio;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicTest
{
    public class PortfolioSamplesControllerTests
    {

        private Mock<PortfolioSamplesDataProvider> GetDataProvider()
        {
            List<PortfolioSample> PortfolioSampleContainer = new List<PortfolioSample>();
            List<Supplier> SupplierContainer = new List<Supplier>();
            Supplier sup = new Supplier() { Id = 1 };
            SupplierContainer.Add(sup);
            var dataProvider = new Mock<PortfolioSamplesDataProvider>();
            dataProvider.Setup(provider => provider.GetPortfolioSample(It.Is<PortfolioSample>(u => PortfolioSampleContainer.Contains(u))))
                .Returns((PortfolioSample u) => PortfolioSampleContainer.First(u.Equals));
            dataProvider.Setup(provider => provider.AddPortfolioSample(It.IsAny<PortfolioSample>(), It.IsAny<Supplier>())).Callback<PortfolioSample, Supplier>((port, suppToAdd) => AddPortfolioSample(PortfolioSampleContainer, SupplierContainer, suppToAdd, port));
            dataProvider.Setup(provider => provider.ModifyPortfolioSample(It.IsAny<PortfolioSample>(), It.IsAny<PortfolioSample>()))
               .Callback<PortfolioSample, PortfolioSample>((PortfolioSampleToModify, newPortfolioSample) => ModifyPortfolioSample(PortfolioSampleContainer, PortfolioSampleToModify, newPortfolioSample));
            dataProvider.Setup(provider => provider.RemovePortfolioSample(It.IsAny<PortfolioSample>())).Callback<PortfolioSample>(d => PortfolioSampleContainer.Remove(d));
            dataProvider.Setup(provider => provider.GetPortfolioSamplesForSupplier(It.IsAny<Supplier>())).Returns<Supplier>((supplierToCheck) => GetPortfolioSamplesForSupplier(SupplierContainer, supplierToCheck));
            return dataProvider;
        }

        private List<PortfolioSample> GetPortfolioSamplesForSupplier(List<Supplier> Suppliers, Supplier sup)
        {
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    return s.Portfolio.ToList();
                }
            }
            return null;
        }

        private void AddPortfolioSample(List<PortfolioSample> PortfolioSamples, List<Supplier> Suppliers, Supplier sup, PortfolioSample port)
        {
            PortfolioSamples.Add(port);
            foreach (Supplier s in Suppliers)
            {
                if (s.Id == sup.Id)
                {
                    s.Portfolio.Add(port);
                }
            }
        }

        private PortfolioSample ModifyPortfolioSample(List<PortfolioSample> PortfolioSamples, PortfolioSample PortfolioSampleToModify, PortfolioSample newPortfolioSample)
        {
            PortfolioSample realPortfolioSampleToModify = PortfolioSamples.First();
            realPortfolioSampleToModify.Title = newPortfolioSample.Title;
            realPortfolioSampleToModify.Description = newPortfolioSample.Description;
            return realPortfolioSampleToModify;
        }

        [Fact]
        public void TestAddPortfolioSampleNullPortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            Assert.Throws<ArgumentException>(() => controller.AddPortfolioSample(null, sup));
        }

        [Fact]
        public void TestAddPortfolioSampleNoTitle()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10 };
            Assert.Throws<ArgumentException>(() => controller.AddPortfolioSample(port, sup));
        }

        [Fact]
        public void TestAddPortfolioSampleNullDescription()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo" };
            Assert.Throws<ArgumentException>(() => controller.AddPortfolioSample(port, sup));
        }

        [Fact]
        public void TestAddPortfolioSampleFutureDate()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo", Description = "Desc", Date = new DateTime(2018, 7, 7) };
            Assert.Throws<ArgumentException>(() => controller.AddPortfolioSample(port, sup));
        }

        [Fact]
        public void TestAddPortfolioSampleNullSupplier()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo", Description = "Desc", Date = new DateTime(2016, 6, 6) };
            Assert.Throws<ArgumentException>(() => controller.AddPortfolioSample(port, null));
        }

        [Fact]
        public void TestAddPortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo", Description = "Desc", Date = new DateTime(2016, 6, 6) };
            controller.AddPortfolioSample(port, sup);
            Assert.Equal(port.Title, controller.GetPortfolioSample(port).Title);
        }

        [Fact]
        public void TestGetNullPortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetPortfolioSample(null));
        }

        [Fact]
        public void TestGetPortfolioSamplesFromSupplierNullSupplier()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.GetPortfolioSamplesForSupplier(null));
        }

        [Fact]
        public void TestGetPortfolioSamplesFromSupplier()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo", Description = "Desc", Date = new DateTime(2016, 6, 6) };
            controller.AddPortfolioSample(port, sup);
            IList<PortfolioSample> PortfolioSamples = controller.GetPortfolioSamplesForSupplier(sup);
            Assert.Equal(PortfolioSamples.First(), port);
        }

        [Fact]
        public void TestRemovePortfolioSampleNullPortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.RemovePortfolioSample(null));
        }

        [Fact]
        public void TestRemovePortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo", Description = "Desc", Date = new DateTime(2016, 6, 6) };
            controller.AddPortfolioSample(port, sup);
            Assert.Equal(port.Title, controller.GetPortfolioSample(port).Title);
            controller.RemovePortfolioSample(port);
            Assert.Null(controller.GetPortfolioSample(port));
        }

        [Fact]
        public void TestModifyPortfolioSampleNullPortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Assert.Throws<ArgumentException>(() => controller.ModifyPortfolioSample(null, null));
        }

        [Fact]
        public void TestModifyPortfolioSample()
        {
            var provider = GetDataProvider().Object;
            PortfolioSamplesController controller = new PortfolioSamplesController();
            controller.DataProvider = provider;
            Supplier sup = new Supplier() { Id = 1 };
            PortfolioSample port = new PortfolioSample() { Id = 10, Title = "Titulo", Description = "Desc", Date = new DateTime(2016, 6, 6) };
            controller.AddPortfolioSample(port, sup);
            PortfolioSample modifiedport = new PortfolioSample() { Id = 10, Title = "Nuevo Titulo", Description = "Desc", Date = new DateTime(2016, 6, 6) };
            controller.ModifyPortfolioSample(port, modifiedport);
            Assert.Equal(modifiedport.Title, controller.GetPortfolioSample(port).Title);
        }
    }
}

