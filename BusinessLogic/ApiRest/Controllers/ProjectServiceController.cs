﻿using ApiRest.DTO;
using ApiRest.GCM;
using BusinessLogic.Image;
using BusinessLogic.Projects;
using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Utilities;

namespace ApiRest.Controllers
{
    public class ProjectServiceController : BaseApiController
    {

        private ProjectsController projectsController;

        public ProjectsController ProjectsController
        {
            get
            {
                if (projectsController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    ProjectPersistentStore persistentStore = new ProjectPersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    projectsController = new ProjectsController();
                    projectsController.DataProvider = persistentStore;
                }
                return projectsController;
            }
            set
            {
                projectsController = value;
            }
        }

        [Route("api/clients/{id}/projects")]
        [HttpGet]
        public IHttpActionResult GetClientProjects(int id)
        {
            return AuthorizeClient(authorizedClient =>
            {
                try
                {
                    List<Project> projects = ProjectsController.GetClientProjects(new Client() { Id = id });
                    var theProjects = projects.Select(project => new
                    {
                        id = project.Id,
                        title = project.Title,
                        description = project.Description,
                        status = project.Status.Description(),
                        budget = project.Budget,
                        deadline = project.Deadline,
                        client = new
                        {
                            id = project.Client.Id,
                            name = project.Client.Name,
                            email = project.Client.Email,
                            phone = project.Client.Phone
                        },
                        suppliers = project.Suppliers.Select(f => new { id = f.Id, name = f.Name, email = f.Email }).ToList(),
                        ratings = project.Ratings.Select(r => new
                        {
                            supplier = new { id = r.Supplier.Id },
                            clientRate = r.ClientRate,
                            supplierRate = r.SupplierRate
                        }).ToList(),
                        location = new
                        {
                            latitude = project.Location.Latitude,
                            longitude = project.Location.Longitude
                        }
                    });
                    return Ok(theProjects.ToList());
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/suppliers/{id}/projects")]
        [HttpGet]
        public IHttpActionResult GetSuppliersProjects(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    List<Project> projects = ProjectsController.GetSupplierProjects(new Supplier() { Id = id });
                    var theProjects = projects.Select(project => new
                    {
                        id = project.Id,
                        title = project.Title,
                        description = project.Description,
                        status = project.Status.Description(),
                        budget = project.Budget,
                        deadline = project.Deadline,
                        client = new
                        {
                            id = project.Client.Id,
                            name = project.Client.Name,
                            email = project.Client.Email,
                            phone = project.Client.Phone
                        },
                        suppliers = project.Suppliers.Select(f => new { id = f.Id, name = f.Name, email = f.Email }).ToList(),
                        ratings = project.Ratings.Select(r => new
                        {
                            supplier = new { id = r.Supplier.Id },
                            clientRate = r.ClientRate,
                            supplierRate = r.SupplierRate
                        }).ToList(),
                        location = new
                        {
                            latitude = project.Location.Latitude,
                            longitude = project.Location.Longitude
                        }
                    });
                    return Ok(theProjects.ToList());
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/projects")]
        [HttpGet]
        public IHttpActionResult GetProjects()
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    List<Project> projects = ProjectsController.GetAvailableProjects(authorizedSupplier);
                    var theProjects = projects.Select(project => new
                    {
                        id = project.Id,
                        title = project.Title,
                        description = project.Description,
                        status = project.Status.Description(),
                        budget = project.Budget,
                        deadline = project.Deadline,
                        client = new
                        {
                            id = project.Client.Id,
                            name = project.Client.Name,
                            email = project.Client.Email,
                            phone = project.Client.Phone
                        },
                        suppliers = project.Suppliers.Select(f => new { id = f.Id, name = f.Name, email = f.Email }).ToList(),
                        ratings = project.Ratings.Select(r => new
                        {
                            supplier = new { id = r.Supplier.Id },
                            clientRate = r.ClientRate,
                            supplierRate = r.SupplierRate
                        }).ToList(),
                        location = new
                        {
                            latitude = project.Location.Latitude,
                            longitude = project.Location.Longitude
                        }
                    });
                    return Ok(theProjects.ToList());
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
        [Route("api/projects/inrange")]
        [HttpPost]
        public IHttpActionResult GetProjectsByDistance([FromBody]ProjectLocationDTO location)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    List<Project> projects = ProjectsController.GetProjectsByDistance(location.Latitude, location.Longitude, location.Distance, authorizedSupplier);
                    var theProjects = projects.Select(project => new
                    {
                        id = project.Id,
                        title = project.Title,
                        description = project.Description,
                        status = project.Status.Description(),
                        budget = project.Budget,
                        deadline = project.Deadline,
                        client = new
                        {
                            id = project.Client.Id,
                            name = project.Client.Name,
                            email = project.Client.Email,
                            phone = project.Client.Phone
                        },
                        suppliers = project.Suppliers.Select(f => new { id = f.Id, name = f.Name, email = f.Email }).ToList(),
                        ratings = project.Ratings.Select(r => new
                        {
                            supplier = new { id = r.Supplier.Id },
                            clientRate = r.ClientRate,
                            supplierRate = r.SupplierRate
                        }).ToList(),
                        location = new
                        {
                            latitude = project.Location.Latitude,
                            longitude = project.Location.Longitude
                        }
                    });
                    return Ok(theProjects.ToList());
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/projects/{id}")]
        [HttpGet]
        public IHttpActionResult GetProject(int id)
        {
            return AuthorizeUser(authorizedUser =>
            {
                try
                {
                    Project project = ProjectsController.GetProject(new Project() { Id = id });
                    var result = new
                    {
                        id = project.Id,
                        title = project.Title,
                        description = project.Description,
                        status = project.Status.Description(),
                        budget = project.Budget,
                        deadline = project.Deadline,
                        client = new
                        {
                            id = project.Client.Id,
                            name = project.Client.Name,
                            email = project.Client.Email,
                            phone = project.Client.Phone
                        },
                        suppliers = project.Suppliers.Select(f => new { id = f.Id, name = f.Name, email = f.Email }).ToList(),
                        ratings = project.Ratings.Select(r => new
                        {
                            supplier = new { id = r.Supplier.Id },
                            clientRate = r.ClientRate,
                            supplierRate = r.SupplierRate
                        }).ToList(),
                        location = new
                        {
                            latitude = project.Location.Latitude,
                            longitude = project.Location.Longitude
                        }
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

       

        [Route("api/projects")]
        [HttpPost]
        public IHttpActionResult AddProject([FromBody] Project projectToAdd)
        {
            return AuthorizeClient(authorizedClient =>
            {
                try
                {
                    projectToAdd.Client = authorizedClient;
                    Project project = ProjectsController.AddProject(projectToAdd);
                    var result = new
                    {
                        id = project.Id,
                        title = project.Title,
                        description = project.Description,
                        status = project.Status.Description(),
                        budget = project.Budget,
                        deadline = project.Deadline,
                        client = new
                        {
                            id = project.Client.Id,
                            name = project.Client.Name,
                            email = project.Client.Email,
                            phone = project.Client.Phone
                        },
                        suppliers = project.Suppliers.Select(f => new { id = f.Id, name = f.Name }).ToList(),
                        location = new
                        {
                            latitude = project.Location.Latitude,
                            longitude = project.Location.Longitude
                        }
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/projects/{id}")]
        [HttpPut]
        public IHttpActionResult ModifyProject(int id, [FromBody] Project newProject)
        {
            return AuthorizeClient(authorizedClient =>
            {
                try
                {
                    Project project = ProjectsController.GetProject(new Project() { Id = id });
                    newProject.Client = authorizedClient;

                    if (project.Client.Id != authorizedClient.Id)
                        return UnauthorizedResult();

                    ProjectsController.ModifyProject(project, newProject);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/projects/{id}/finalize")]
        [HttpPut]
        public IHttpActionResult FinalizeProject(int id, [FromBody] IList<ProjectRating> projectRatings)
        {
            return AuthorizeClient(authorizedClient =>
            {
                try
                {
                    ProjectsController.FinalizeProject(new Project() { Id = id }, projectRatings);
                    Project realProject = ProjectsController.GetProject(new Project() { Id = id });
                    realProject.Suppliers.ToList().ForEach(user =>
                    {
                        SendNotification(user, realProject, new NotificationData()
                        {
                            Type = NotificationType.FinalizedProject,
                            Title = "Proyecto finalizado",
                            Description = "El proyecto " + realProject.Title + " ha terminado. Califica al cliente.",
                            Client = realProject.Client
                        });
                    });
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/projects/{id}/rate_client")]
        [HttpPost]
        public IHttpActionResult RateClient(int id, [FromBody] RateClientDTO rateClientDTO)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    ProjectsController.RateClient(authorizedSupplier, new Project() { Id = id }, rateClientDTO.Rating);
                    Project realProject = ProjectsController.GetProject(new Project() { Id = id });
                    SendNotification(realProject.Client, realProject, new NotificationData()
                    {
                        Type = NotificationType.ClientRated,
                        Title = "Calificación recibida",
                        Description = authorizedSupplier.Name + " te ha calificado con " + rateClientDTO.Rating + " estrellas en el proyecto " + realProject.Title + "."
                    });
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
        [Route("api/projects/applay")]
        [HttpPut]
        public IHttpActionResult ApplayProjects([FromBody] IList<Project> projects)
        {
            return AuthorizeSupplier(authorizeSupplier =>
            {
                try
                {
                    ProjectsController.ProjectsApplay(authorizeSupplier, projects);

                    foreach( Project p in projects)
                    {
                        Project realProject = ProjectsController.GetProject(p);
                        List<User> members = new List<User> { realProject.Client };
                        members = members.Concat(realProject.Suppliers).ToList();
                        members.ForEach(user =>
                        {
                            SendNotification(user, realProject, new NotificationData()
                            {
                                Type = NotificationType.ApprovedProject,
                                Title = "Proyecto aceptado",
                                Description = "El proyecto " + realProject.Title + " ha sido aceptado."
                            });
                        });
                    }

                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/projects/{id}/image")]
        [HttpPut]
        public async Task<IHttpActionResult> AddPhoto(int id)
        {
            try
            {
                Project project = new Project() { Id = id };
                var provider = new MultipartMemoryStreamProvider();
                var files = await Request.Content.ReadAsMultipartAsync();
                var image = files.Contents[0];
                ValidateImage(image);
                var imageData = await image.ReadAsByteArrayAsync();

                string fileName = Guid.NewGuid().ToString();

                ImageDefinition imageToAdd = new ImageDefinition()
                {
                    ContentType = image.Headers.ContentType.ToString(),
                    FileName = fileName
                };

                SaveImageToDisk(fileName, imageData);

                ProjectsController.ModifyPhoto(project, imageToAdd);
                var result = new
                {
                    success = true
                };
                return Ok(result);
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        [Route("api/projects/{id}/image")]
        [HttpGet]
        public IHttpActionResult GetPhoto(int id)
        {
            return AuthorizeUser(authorizedUser =>
            {
                try
                {
                    Project project = new Project() { Id = id };
                    ImageDefinition imageToSend = ProjectsController.GetProject(project).Photo;
                    
                    if (imageToSend == null)
                        throw new ArgumentException("El usuario no tiene imagen de perfil");

                    byte[] data = LoadImageFromDisk(imageToSend.FileName);
                    return new FileResponse(imageToSend.ContentType, data);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
    }

}
