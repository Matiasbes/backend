﻿using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class ClientServiceController : BaseApiController
    {
        private ClientsController clientsController;

        public ClientsController ClientsController
        {
            get
            {
                if (clientsController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    ClientPersistentStore persistentStore = new ClientPersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    clientsController = new ClientsController();
                    clientsController.DataProvider = persistentStore;
                }
                return clientsController;
            }
            set
            {
                clientsController = value;
            }
        }

        [Route("api/clients/{id}")]
        [HttpGet]
        public IHttpActionResult GetClient(int id)
        {
            return AuthorizeUser(authorizedUser =>
            {
                try
                {
                    Client client = UsersController.GetClientFromUser(new User() { Id = id });
                    var result = new
                    {
                        id = client.Id,
                        name = client.Name,
                        email = client.Email,
                        facebookId = client.FacebookId,
                        phone = client.Phone
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/clients/{id}")]
        [HttpPut]
        public IHttpActionResult ModifyClient(int id, [FromBody] Client client)
        {

            return AuthorizeClient(authorizedClient =>
            {
                if (authorizedClient.Id != id)
                    return UnauthorizedResult();
                try
                {
                    ClientsController.ModifyClient(authorizedClient, client);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
    }
}