﻿using BusinessLogic.Users;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Jobs;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class JobServiceController : BaseApiController
    {
        private JobsController jobsController;

        public JobsController JobsController
        {
            get
            {
                if (jobsController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    JobPersistentStore persistentStore = new JobPersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    jobsController = new JobsController();
                    jobsController.DataProvider = persistentStore;
                }
                return jobsController;
            }
            set
            {
                jobsController = value;
            }
        }

        [Route("api/suppliers/{supplierId}/jobs")]
        [HttpGet]
        public IHttpActionResult GetJobs(int suppliersId)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != suppliersId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(JobsController.GetJobsForSupplier(authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/suppliers/{supplierId}/jobs")]
        [HttpPost]
        public IHttpActionResult AddJob(int supplierId, [FromBody] Job job)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != supplierId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(JobsController.AddJob(job, authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/jobs/{id}")]
        [HttpGet]
        public IHttpActionResult GetJob(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Job jobToRetrieve = new Job() { Id = id };
                    Job retrievedJob = JobsController.GetJob(jobToRetrieve);
                    if (!authorizedSupplier.Jobs.Any<Job>(d => d.Id == retrievedJob.Id))
                        return UnauthorizedResult();
                    return Ok(retrievedJob);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/jobs/{id}")]
        [HttpPut]
        public IHttpActionResult ModifyJob(int id, [FromBody] Job newJob)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Job jobToModify = new Job() { Id = id };
                    Job retrievedJob = JobsController.GetJob(jobToModify);
                    if (!authorizedSupplier.Jobs.Any<Job>(d => d.Id == retrievedJob.Id))
                        return UnauthorizedResult();
                    return Ok(JobsController.ModifyJob(jobToModify, newJob));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/jobs/{id}")]
        [HttpDelete]
        public IHttpActionResult RemoveJob(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Job jobToRemove = new Job() { Id = id };
                    Job retrievedJob = JobsController.GetJob(jobToRemove);
                    if (!authorizedSupplier.Jobs.Any<Job>(d => d.Id == retrievedJob.Id))
                        return UnauthorizedResult();
                    JobsController.RemoveJob(jobToRemove);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
    }
}
