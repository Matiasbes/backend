﻿using BusinessLogic.Users;
using BusinessLogic.Users.Suppliers;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class SupplierServiceController : BaseApiController
    {
        private SuppliersController supplierController;

        public SuppliersController SuppliersController
        {
            get
            {
                if (supplierController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    SupplierPersistentStore persistentStore = new SupplierPersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    supplierController = new SuppliersController();
                    supplierController.DataProvider = persistentStore;
                }
                return supplierController;
            }
            set
            {
                supplierController = value;
            }
        }

        [Route("api/suppliers/{id}")]
        [HttpGet]
        public IHttpActionResult GetSupplier(int id)
        {
            return AuthorizeUser(authorizedUser =>
            {
                try
                {
                    Supplier supplier = UsersController.GetSupplierFromUser(new User() { Id = id });
                    var result = new
                    {
                        id = supplier.Id,
                        name = supplier.Name,
                        email = supplier.Email,
                        facebookId = supplier.FacebookId,
                        phone = supplier.Phone,
                        overview = supplier.Overview,
                        rate = supplier.Rate,
                        degrees = supplier.Degrees,
                        jobs = supplier.Jobs,
                        skills = supplier.Skills,
                        portfolio = supplier.Portfolio
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/suppliers/{id}")]
        [HttpPut]
        public IHttpActionResult ModifySupplier(int id, [FromBody] Supplier supplier)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != id)
                    return UnauthorizedResult();
                try
                {
                    Supplier supplierToModify = UsersController.GetSupplierFromUser(new User() { Id = id });
                    SuppliersController.ModifySupplier(supplierToModify, supplier);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

    }
}
