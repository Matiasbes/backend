﻿using ApiRest.GCM;
using BusinessLogic.Authentication;
using BusinessLogic.Projects;
using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class ErrorMessage
    {

        public string Message { get; set; }

    }

    public class BaseApiController : ApiController
    {
        public static string AuthorizationHeader = "Authorization";
        public static string AuthorizationTokenPrefix = "Basic ";
        private UsersController usersController;

        public UsersController UsersController
        {
            get
            {
                if (usersController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    UserPersistentStore persistentStore = new UserPersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    usersController = new UsersController();
                    usersController.DataProvider = persistentStore;
                }
                return usersController;
            }
            set
            {
                usersController = value;
            }
        }

        public IHttpActionResult OkResult(string message)
        {
            return Content(HttpStatusCode.OK, new ErrorMessage() { Message = message });
        }

        public IHttpActionResult NotFoundResult(string message)
        {
            return Content(HttpStatusCode.NotFound, new ErrorMessage() { Message = message });
        }

        public IHttpActionResult BadRequestResult(string message)
        {
            return Content(HttpStatusCode.BadRequest, new ErrorMessage() { Message = message });
        }

        public IHttpActionResult InternalServerErrorResult()
        {
            return InternalServerErrorResult("Error al procesar la solicitud");
        }

        public IHttpActionResult InternalServerErrorResult(string message)
        {
            return Content(HttpStatusCode.InternalServerError, new ErrorMessage() { Message = message });
        }

        public virtual IHttpActionResult ServiceUnavailableResult(string message)
        {
            return Content(HttpStatusCode.ServiceUnavailable, new ErrorMessage() { Message = message });
        }
        public IHttpActionResult UnauthorizedResult()
        {
            return Content(HttpStatusCode.Unauthorized, new ErrorMessage() { Message = "Acceso denegado. No tiene los permisos suficientes para realizar la acción." });
        }
        public Session GetSessionFromHeader()
        {
            try
            {
                IEnumerable<string> values = Request.Headers.GetValues(AuthorizationHeader);
                string header = values.First();
                string token = header.Substring(AuthorizationTokenPrefix.Count());
                return new Session() { Token = token };
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("No se encontró el token de autorización", e);
            }
        }

        public IHttpActionResult AuthorizeUser(Func<User, IHttpActionResult> authorizedUserCallback)
        {
            User user;

            try
            {
                user = GetUserFromHeaderSession();
            }
            catch (Exception)
            {
                return UnauthorizedResult();
            }

            return authorizedUserCallback(user);
        }

        private User GetUserFromHeaderSession()
        {
            Session session = GetSessionFromHeader();
            User user = UsersController.GetUserFromSession(session);
            return user;
        }

        public IHttpActionResult AuthorizeClient(Func<Client, IHttpActionResult> authorizedClientCallback)
        {
            Client client;

            try
            {
                User user = GetUserFromHeaderSession();
                client = usersController.GetClientFromUser(user);
            }
            catch (Exception)
            {
                return UnauthorizedResult();
            }

            return authorizedClientCallback(client);
        }

        public IHttpActionResult AuthorizeSupplier(Func<Supplier, IHttpActionResult> authorizedSupplierCallback)
        {
            Supplier supplier;

            try
            {
                User user = GetUserFromHeaderSession();
                supplier = usersController.GetSupplierFromUser(user);
            }
            catch (Exception)
            {
                return UnauthorizedResult();
            }

            return authorizedSupplierCallback(supplier);
        }
        protected void ValidateImage(HttpContent image)
        {
            if (image.Headers.ContentLength == 0)
                throw new ArgumentException("La imagen recibida es vacía");

            if (image.Headers.ContentType == null)
                throw new ArgumentException("No especifico el tipo de la imagen");
        }

        public virtual void SaveImageToDisk(string fileName, byte[] imageData)
        {
            string path = HttpContext.Current.Server.MapPath("~/App_Data/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string filePath = Path.Combine(path, fileName + ".jpg");
            File.WriteAllBytes(filePath, imageData);
        }

        public virtual byte[] LoadImageFromDisk(string fileName)
        {
            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data"), fileName + ".jpg");
            return File.ReadAllBytes(filePath);
        }

        public void SendNotification(User user, NotificationData notificationData)
        {
            GCMNotificationManager.SendNotification(user, notificationData);
        }

        public void SendNotification(User user, Project project, NotificationData notificationData)
        {
            GCMNotificationManager.SendNotification(user, project, notificationData);
        }
    }
}

