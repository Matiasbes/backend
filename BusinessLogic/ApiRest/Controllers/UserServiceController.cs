﻿using ApiRest.DTO;
using BusinessLogic.Authentication;
using BusinessLogic.Image;
using BusinessLogic.Users;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Utilities;

namespace ApiRest.Controllers
{
    public class UserServiceController : BaseApiController
    {

        [Route("api/users")]
        [HttpPost]
        public IHttpActionResult Signup([FromBody] UserDTO user)
        {
            try
            {
                User createdUser = UsersController.AddUser(user.BusinessLogicRepresentation());
                UsersController.AddDeviceId(createdUser, new DeviceId() { DeviceToken = user.DeviceId });
                Session session = UsersController.NewSessionForUser(createdUser);
                var result = new
                {
                    id = createdUser.Id,
                    name = createdUser.Name,
                    email = createdUser.Email,
                    type = user.Type,
                    facebookId = createdUser.FacebookId,
                    phone = createdUser.Phone,
                    token = session.Token
                };
                return Ok(result);
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        [Route("api/sessions")]
        [HttpPost]
        public IHttpActionResult Login([FromBody] UserDTO user)
        {
            if (user.IsNull())
                return BadRequestResult("Se debe indicar el email y contraseña");

            try
            {
                User realUser = UsersController.GetUserWithCredentials(user.Email, user.Password);
                UsersController.AddDeviceId(realUser, new DeviceId() { DeviceToken  = user.DeviceId});
                Session session = UsersController.NewSessionForUser(realUser);
                var result = new
                {
                    id = realUser.Id,
                    name = realUser.Name,
                    email = realUser.Email,
                    type = GetUserType(realUser),
                    facebookId = realUser.FacebookId,
                    phone = realUser.Phone,
                    token = session.Token
                };
                return Ok(result);
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        private UserType GetUserType(User user)
        {
            try
            {
                UsersController.GetSupplierFromUser(user);
                return UserType.Supplier;
            }
            catch (PersistentStoreException)
            {
                throw;
            }
            catch (Exception)
            {
                return UserType.Client;
            }
        }

        [Route("api/users/{id}/password")]
        [HttpPut]
        public IHttpActionResult ChangePassword(int id, [FromBody] UserChangePasswordDTO userDTO)
        {
            return AuthorizeUser(authorizedUser =>
            {
                try
                {
                    return Ok(UsersController.ChangePassword(new User() { Id = id }, userDTO.ActualPassword, userDTO.NewPassword));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
            }
        [Route("api/users/facebook/{id}")]
        [HttpPut]
        public IHttpActionResult GetFacebookUser(string id, [FromBody] FacebookUserDTO userDTO)
        {
            try
            {
                User facebookUser = UsersController.GetUserWithFacebookId(id);
                Session session = UsersController.NewSessionForUser(facebookUser);
                var result = new
                {
                    id = facebookUser.Id,
                    name = facebookUser.Name,
                    email = facebookUser.Email,
                    type = GetUserType(facebookUser),
                    token = session.Token,
                    phone = facebookUser.Phone,
                    facebookId = facebookUser.FacebookId
                };
                return Ok(result);
            }
            catch (FacebookUserNotFoundException)
            {
                if (UsersController.ExistsUserWithEmail(userDTO.Email))
                {
                    try
                    {
                        User user = UsersController.SyncUserWithFacebook(userDTO.Email, id);
                        Session session = UsersController.NewSessionForUser(user);
                        var result = new
                        {
                            id = user.Id,
                            name = user.Name,
                            email = user.Email,
                            type = GetUserType(user),
                            token = session.Token,
                            phone = user.Phone,
                        };
                        return Ok(result);
                    }
                    catch (PersistentStoreException e)
                    {
                        return ServiceUnavailableResult(e.Message);
                    }
                }
                else
                    return BadRequestResult("No hay ningún usuario asociado al email de Facebook. Debe registrarse primero.");
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        [Route("api/users/facebook")]
        [HttpPost]
        public IHttpActionResult SignupFacebookUser([FromBody] UserDTO userDTO)
        {
            try
            {
                User createdUser = UsersController.AddUserSyncedWithFacebook(userDTO.BusinessLogicRepresentation());
                Session session = UsersController.NewSessionForUser(createdUser);
                var result = new
                {
                    id = createdUser.Id,
                    name = createdUser.Name,
                    email = createdUser.Email,
                    phone = createdUser.Phone,
                    type = userDTO.Type,
                    token = session.Token,
                    facebookId = createdUser.FacebookId
                };
                return Ok(result);
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        [Route("api/users/{id}/rating")]
        [HttpGet]
        public IHttpActionResult GetUserRating(int id)
        {
            return AuthorizeUser(authorizedUser =>
            {
                try
                {
                    User user = new User() { Id = id };
                    return Ok(new { rating = UsersController.GetUserRating(user) });
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/users/{id}/profileimage")]
        [HttpPut]
        public async Task<IHttpActionResult> AddProfilePhoto(int id)
        {
            try
            {
                User user = new User() { Id = id };
                var provider = new MultipartMemoryStreamProvider();
                var files = await Request.Content.ReadAsMultipartAsync();
                var image = files.Contents[0];
                ValidateImage(image);
                var imageData = await image.ReadAsByteArrayAsync();

                string fileName = Guid.NewGuid().ToString();

                ImageDefinition imageToAdd = new ImageDefinition()
                {
                    ContentType = image.Headers.ContentType.ToString(),
                    FileName = fileName
                };

                SaveImageToDisk(fileName, imageData);

                UsersController.ModifyProfilePhoto(user, imageToAdd);
                var result = new
                {
                    success = true
                };
                return Ok(result);
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        [Route("api/users/{id}/profileimage")]
        [HttpGet]
        public IHttpActionResult GetProfilePhoto(int id)
        {
           
                try
                {
                    User user = UsersController.GetUser(new User() { Id = id });
                    ImageDefinition imageToSend = user.ProfilePhoto;

                    if (imageToSend == null)
                        throw new ArgumentException("El usuario no tiene imagen de perfil");

                    byte[] data = LoadImageFromDisk(imageToSend.FileName);
                    return new FileResponse(imageToSend.ContentType, data);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
        }
    }
}
