﻿using BusinessLogic.Image;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Portfolio;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class PortfolioSampleServiceController : BaseApiController
    {

        private PortfolioSamplesController portfolioSamplesController;

        public PortfolioSamplesController PortfolioSamplesController
        {
            get
            {
                if (portfolioSamplesController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    PortfolioSamplePersistentStore persistentStore = new PortfolioSamplePersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    portfolioSamplesController = new PortfolioSamplesController();
                    portfolioSamplesController.DataProvider = persistentStore;
                }
                return portfolioSamplesController;
            }
            set
            {
                portfolioSamplesController = value;
            }
        }

        [Route("api/suppliers/{supplierId}/portfolio")]
        [HttpGet]
        public IHttpActionResult GetPortfolioSamples(int supplierId)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != supplierId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(PortfolioSamplesController.GetPortfolioSamplesForSupplier(authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/suppliers/{supplierId}/portfolio")]
        [HttpPost]
        public IHttpActionResult AddPortfolioSample(int supplierId, [FromBody] PortfolioSample portfolioSample)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != supplierId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(PortfolioSamplesController.AddPortfolioSample(portfolioSample, authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/portfolio/{id}")]
        [HttpGet]
        public IHttpActionResult GetPortfolioSample(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    PortfolioSample portfolioSampleToRetrieve = new PortfolioSample() { Id = id };
                    PortfolioSample retrievedPortfolioSample = PortfolioSamplesController.GetPortfolioSample(portfolioSampleToRetrieve);
                    if (!authorizedSupplier.Portfolio.Any<PortfolioSample>(d => d.Id == retrievedPortfolioSample.Id))
                        return UnauthorizedResult();
                    return Ok(retrievedPortfolioSample);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/portfolio/{id}")]
        [HttpPut]
        public IHttpActionResult ModifyPortfolioSample(int id, [FromBody] PortfolioSample newPortfolioSample)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    PortfolioSample portfolioSampleToModify = new PortfolioSample() { Id = id };
                    PortfolioSample retrievedPortfolioSample = PortfolioSamplesController.GetPortfolioSample(portfolioSampleToModify);
                    if (!authorizedSupplier.Portfolio.Any<PortfolioSample>(d => d.Id == retrievedPortfolioSample.Id))
                        return UnauthorizedResult();
                    return Ok(PortfolioSamplesController.ModifyPortfolioSample(portfolioSampleToModify, newPortfolioSample));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/portfolio/{id}")]
        [HttpDelete]
        public IHttpActionResult RemovePortfolioSample(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    PortfolioSample portfolioSampleToRemove = new PortfolioSample() { Id = id };
                    PortfolioSample retrievedPortfolioSample = PortfolioSamplesController.GetPortfolioSample(portfolioSampleToRemove);
                    if (!authorizedSupplier.Portfolio.Any<PortfolioSample>(d => d.Id == retrievedPortfolioSample.Id))
                        return UnauthorizedResult();
                    PortfolioSamplesController.RemovePortfolioSample(portfolioSampleToRemove);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
        [Route("api/portfolio/{id}/image")]
        [HttpPut]
        public async Task<IHttpActionResult> AddPhoto(int id)
        {
            try
            {
                PortfolioSample portfolioSample = new PortfolioSample() { Id = id };
                var provider = new MultipartMemoryStreamProvider();
                var files = await Request.Content.ReadAsMultipartAsync();
                var image = files.Contents[0];
                ValidateImage(image);
                var imageData = await image.ReadAsByteArrayAsync();

                string fileName = Guid.NewGuid().ToString();

                ImageDefinition imageToAdd = new ImageDefinition()
                {
                    ContentType = image.Headers.ContentType.ToString(),
                    FileName = fileName
                };

                SaveImageToDisk(fileName, imageData);

                PortfolioSamplesController.ModifyPhoto(portfolioSample, imageToAdd);
                var result = new
                {
                    success = true
                };
                return Ok(result);
            }
            catch (PersistentStoreException e)
            {
                return ServiceUnavailableResult(e.Message);
            }
            catch (Exception e)
            {
                return BadRequestResult(e.Message);
            }
        }

        [Route("api/portfolio/{id}/image")]
        [HttpGet]
        public IHttpActionResult GetPhoto(int id)
        {
            {
                try
                {
                    PortfolioSample portfolioSample = new PortfolioSample() { Id = id };
                    ImageDefinition imageToSend = PortfolioSamplesController.GetPortfolioSample(portfolioSample).Photo;

                    if (imageToSend == null)
                        throw new ArgumentException("El usuario no tiene imagen de perfil");

                    byte[] data = LoadImageFromDisk(imageToSend.FileName);
                    return new FileResponse(imageToSend.ContentType, data);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            }
        }
    }
}
