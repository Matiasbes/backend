﻿using BusinessLogic.Users;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Skills;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class SkillServiceController : BaseApiController
    {
        private SkillsController skillsController;

        public SkillsController SkillsController
        {
            get
            {
                if (skillsController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    SkillPersistentStore persistentStore = new SkillPersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    skillsController = new SkillsController();
                    skillsController.DataProvider = persistentStore;
                }
                return skillsController;
            }
            set
            {
                skillsController = value;
            }
        }

        [Route("api/suppliers/{supplierId}/skills")]
        [HttpGet]
        public IHttpActionResult GetSkills(int supplierId)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != supplierId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(SkillsController.GetSkillsForSupplier(authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/suppliers/{supplierId}/skills")]
        [HttpPost]
        public IHttpActionResult AddSkill(int supplierId, [FromBody] Skill skill)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != supplierId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(SkillsController.AddSkill(skill, authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/skills/{id}")]
        [HttpGet]
        public IHttpActionResult GetSkill(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Skill skillToRetrieve = new Skill() { Id = id };
                    Skill retrievedSkill = SkillsController.GetSkill(skillToRetrieve);
                    if (!authorizedSupplier.Skills.Any<Skill>(d => d.Id == retrievedSkill.Id))
                        return UnauthorizedResult();
                    return Ok(retrievedSkill);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/skills/{id}")]
        [HttpPut]
        public IHttpActionResult ModifySkill(int id, [FromBody] Skill newSkill)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Skill skillToModify = new Skill() { Id = id };
                    Skill retrievedSkill = SkillsController.GetSkill(skillToModify);
                    if (!authorizedSupplier.Skills.Any<Skill>(d => d.Id == retrievedSkill.Id))
                        return UnauthorizedResult();
                    return Ok(SkillsController.ModifySkill(skillToModify, newSkill));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/skills/{id}")]
        [HttpDelete]
        public IHttpActionResult RemoveSkill(int id)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Skill skillToRemove = new Skill() { Id = id };
                    Skill retrievedSkill = SkillsController.GetSkill(skillToRemove);
                    if (!authorizedSupplier.Skills.Any<Skill>(d => d.Id == retrievedSkill.Id))
                        return UnauthorizedResult();
                    SkillsController.RemoveSkill(skillToRemove);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }
    }
}
