﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class FileResponse : IHttpActionResult
    {
        public FileResponse(string contentType, byte[] fileData)
        {
            ContentType = contentType;
            FileData = fileData;
        }

        public string ContentType { set; get; }

        public byte[] FileData { set; get; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            MemoryStream memoryStream = new MemoryStream(FileData);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(memoryStream);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentType);

            return Task.FromResult(response);
        }
    }
}
