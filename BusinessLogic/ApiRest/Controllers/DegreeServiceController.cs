﻿using BusinessLogic.Users;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Degrees;
using PersistentDatabaseStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRest.Controllers
{
    public class DegreeServiceController : BaseApiController
    {
        private DegreesController degreesController;

        public DegreesController DegreesController
        {
            get
            {
                if (degreesController == null)
                {
                    PersistentStoreContext context = new PersistentStoreContext();
                    DegreePersistentStore persistentStore = new DegreePersistentStore();
                    persistentStore.PersistentStoreContext = context;

                    degreesController = new DegreesController();
                    degreesController.DataProvider = persistentStore;
                }
                return degreesController;
            }
            set
            {
                degreesController = value;
            }
        }

        [Route("api/suppliers/{suppliersId}/degrees")]
        [HttpGet]
        public IHttpActionResult GetDegrees(int suppliersId)
        {
            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != suppliersId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(DegreesController.GetDegreesForSupplier(authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/suppliers/{suppliersId}/degrees")]
        [HttpPost]
        public IHttpActionResult AddDegree(int suppliersId, [FromBody] Degree degree)
        {

            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != suppliersId)
                    return UnauthorizedResult();
                try
                {
                    return Ok(DegreesController.AddDegree(degree, authorizedSupplier));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/degrees/{id}")]
        [HttpGet]
        public IHttpActionResult GetDegree(int id)
        {

            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Degree degreeToRetrieve = new Degree() { Id = id };
                    Degree retrievedDegree = DegreesController.GetDegree(degreeToRetrieve);
                    if (!authorizedSupplier.Degrees.Any<Degree>(d => d.Id == retrievedDegree.Id))
                        return UnauthorizedResult();
                    return Ok(retrievedDegree);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/degrees/{id}")]
        [HttpPut]
        public IHttpActionResult ModifyDegree(int id, [FromBody] Degree newDegree)
        {

            return AuthorizeSupplier(authorizedSupplier =>
            {
                try
                {
                    Degree degreeToModify = new Degree() { Id = id };
                    Degree retrievedDegree = DegreesController.GetDegree(degreeToModify);
                    if (!authorizedSupplier.Degrees.Any<Degree>(d => d.Id == retrievedDegree.Id))
                        return UnauthorizedResult();
                    return Ok(DegreesController.ModifyDegree(retrievedDegree, newDegree));
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

        [Route("api/degrees/{id}")]
        [HttpDelete]
        public IHttpActionResult RemoveDegree(int id)
        {

            return AuthorizeSupplier(authorizedSupplier =>
            {
                if (authorizedSupplier.Id != id)
                    return UnauthorizedResult();
                try
                {
                    Degree degreeToRemove = new Degree() { Id = id };
                    Degree retrievedDegree = DegreesController.GetDegree(degreeToRemove);
                    if (!authorizedSupplier.Degrees.Any<Degree>(d => d.Id == retrievedDegree.Id))
                        return UnauthorizedResult();
                    DegreesController.RemoveDegree(retrievedDegree);
                    var result = new
                    {
                        success = true
                    };
                    return Ok(result);
                }
                catch (PersistentStoreException e)
                {
                    return ServiceUnavailableResult(e.Message);
                }
                catch (Exception e)
                {
                    return BadRequestResult(e.Message);
                }
            });
        }

    }
}
