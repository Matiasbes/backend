﻿using ApiRest.DTO;
using BusinessLogic.Projects;
using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using Newtonsoft.Json.Linq;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;

namespace ApiRest.GCM
{
    public class GCMNotificationManager
    {

        //private static string GCM_SENDER_ID = "886339665909";
        //private static string AUTHENTICATION_TOKEN = "AIzaSyB_GXC5SyAgOub-oxTtjLHiC8EiEGtvvmY";

        public static void SendNotification(User user, NotificationData notificationData)
        {
            SendNotification(user, null, notificationData);
        }

        public static void SendNotification(User user, Project project, NotificationData notificationData)
        {
            //var config = new GcmConfiguration(GCM_SENDER_ID, AUTHENTICATION_TOKEN, null);
            //var gcmBroker = new GcmServiceBroker(config);

            var config = new GcmConfiguration("AAAAzl31__U:APA91bGhMawV7WH521LjTiLV4HOCOgHWUDB1Hw7fOk3aQmT3Nt_KGV4hfTPtI4i01fa5viDDLByJNiKYpjBDEFkfE-VXEhW9Nvl2KeATl-4UQqkW1x5Vf6VgOLOJ-pn0qvGE27cbxFtR");
            config.GcmUrl = "https://fcm.googleapis.com/fcm/send";

            // Create a new broker
            var gcmBroker = new GcmServiceBroker(config);

            gcmBroker.Start();
            user.DevicesIds.Select(device => device.DeviceToken).ToList().ForEach(deviceId =>
            {
                JObject data = JObject.FromObject(new
                {
                    title = notificationData.Title,
                    description = notificationData.Description
                });

                data["rx_gcm_key_target"] = ((int)notificationData.Type).ToString();

                AppendUserData(data, user, "user");
                if (notificationData.Client.IsNotNull())
                    AppendUserData(data, notificationData.Client, "client");
                AppendProjectData(data, project);

                GcmNotification notification = new GcmNotification()
                {
                    RegistrationIds = new List<string> { deviceId },
                    Data = data
                };
                gcmBroker.QueueNotification(notification);
            });
            gcmBroker.Stop();
        }

        private static void AppendUserData(JObject data, User user, string key)
        {
            if (user.IsNull())
                return;
            UserType type = GetUserType(user);
            if (type != UserType.Supplier)
                AppendNormalUserData(data, user, key);
            else
                AppendSupplierData(data, (Supplier)user, key);
        }

        private static void AppendNormalUserData(JObject data, User user, string key)
        {
            data[key] = JObject.FromObject(new
            {
                id = user.Id,
                name = user.Name,
                email = user.Email,
                type = GetUserType(user),
                facebookId = user.FacebookId,
            });
        }

        private static void AppendSupplierData(JObject data, Supplier supplier, string key)
        {
            data[key] = JObject.FromObject(new
            {
                id = supplier.Id,
                name = supplier.Name,
                email = supplier.Email,
                facebookId = supplier.FacebookId,
                overview = supplier.Overview,
                rate = supplier.Rate,
                degrees = supplier.Degrees.Select(degree => JObject.FromObject(new
                {
                    id = degree.Id,
                    institution = degree.Institution,
                    startDate = degree.StartDate,
                    endDate = degree.EndDate,
                    title = degree.Title,
                    description = degree.Description
                })),
                jobs = supplier.Jobs.Select(job => JObject.FromObject(new
                {
                    id = job.Id,
                    enterprise = job.Enterprise,
                    startDate = job.StartDate,
                    endDate = job.EndDate,
                    title = job.Title,
                    description = job.Description
                })),
                skills = supplier.Skills.Select(skill => JObject.FromObject(new
                {
                    id = skill.Id,
                    title = skill.Title
                })),
                portfolio = supplier.Portfolio.Select(portfolio => JObject.FromObject(new
                {
                    id = portfolio.Id,
                    title = portfolio.Title,
                    description = portfolio.Description,
                    url = portfolio.Url,
                    date = portfolio.Date
                })),
                type = UserType.Supplier
            });
        }

        private static void AppendProjectData(JObject data, Project project)
        {
            if (project.IsNull())
                return;
            data["project"] = JObject.FromObject(new
            {
                id = project.Id,
                title = project.Title,
                description = project.Description,
                status = project.Status.Description(),
                budget = project.Budget,
                deadline = project.Deadline,
                client = JObject.FromObject(new
                {
                    id = project.Client.Id,
                    name = project.Client.Name,
                    email = project.Client.Email
                }),
                suppliers = project.Suppliers.Select(f => JObject.FromObject(new { id = f.Id, name = f.Name, email = f.Email })).ToList(),
                ratings = project.Ratings.Select(r => JObject.FromObject(new
                {
                    supplier = new { id = r.Supplier.Id },
                    clientRate = r.ClientRate,
                    supplierRate = r.SupplierRate
                })).ToList()
            });
        }

        private static UserType GetUserType(User user)
        {
            try
            {
                var supplier = (Supplier)user;
                return UserType.Supplier;
            }
            catch (Exception)
            {
                var client = (Client)user;
                return UserType.Client;
            }

        }

    }
}
