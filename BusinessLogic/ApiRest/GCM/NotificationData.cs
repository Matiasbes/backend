﻿using BusinessLogic.Users.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiRest.GCM
{
    public enum NotificationType
    {
        ApprovedProject,
        FinalizedProject,
        ClientRated
    }

    public class NotificationData
    {

        public NotificationType Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Client Client { get; set; }

    }
}
