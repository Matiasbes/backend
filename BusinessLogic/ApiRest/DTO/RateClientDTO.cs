﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRest.DTO
{
    public class RateClientDTO
    {
        public int Rating { get; set; }
    }
}