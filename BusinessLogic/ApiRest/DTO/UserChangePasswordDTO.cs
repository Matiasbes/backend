﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRest.DTO
{
    public class UserChangePasswordDTO
    {

        public string ActualPassword { get; set; }
        public string NewPassword { get; set; }
    }
}