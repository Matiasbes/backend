﻿using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRest.DTO
{
    public enum UserType
    {
        Client,
        Supplier
    }
    public class UserDTO
    {

        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserType Type { get; set; }
        public string FacebookId { get; set; }
        public string DeviceId { get; set; }

        public User BusinessLogicRepresentation()
        {
            switch (Type)
            {
                case UserType.Client:
                    return new Client() { Name = Name, Email = Email, Password = Password, FacebookId = FacebookId };
                default:
                    return new Supplier() { Name = Name, Email = Email, Password = Password, FacebookId  = FacebookId };
            }
        }
    }
}