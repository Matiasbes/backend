﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRest.DTO
{
    public class ProjectLocationDTO
    {
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public Double Distance { get; set; }
    }
}