﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class StringUtilities
    {
        public static bool NullOrEmpty(this string stringToVerify)
        {
            return stringToVerify == null || stringToVerify.Equals("");
        }

    }
}
