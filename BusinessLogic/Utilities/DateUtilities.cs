﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class DateUtilities
    {
        public static bool EqualOrGreaterThan(this DateTime startDate, DateTime endDate)
        {
            return startDate.CompareTo(endDate) >= 0;
        }

        public static bool GreaterThan(this DateTime startDate, DateTime endDate)
        {
            return startDate.CompareTo(endDate) > 0;
        }
    }
}
