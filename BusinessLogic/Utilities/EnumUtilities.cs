﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class EnumUtilities
    {
        public static string Description(this Enum enumValue)
        {
            return GetEnumDescription(enumValue);
        }

        private static string GetEnumDescription(object enumObject)
        {
            var stringField = enumObject.GetType().GetField(enumObject.ToString());
            var attributes = (DescriptionAttribute[])stringField.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : enumObject.ToString();
        }
    }
}
