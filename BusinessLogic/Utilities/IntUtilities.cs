﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class IntUtilities
    {
        public static bool Between(this int value, int min, int max)
        {
            return value >= min && value <= max;
        }
    }
}
