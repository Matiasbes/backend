﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class EmailValidator
    {
        public static bool IsValid(string email)
        {
            if (email.NullOrEmpty())
                return false;
            if (!HasOneAtSign(email))
                return false;
            string[] emailParts = email.Split('@');
            return IsEmailUserValid(emailParts[0]) && IsEmailDomainValid(emailParts[1]);
        }

        private static bool HasOneAtSign(string email)
        {
            return email.Count(source => source == '@') == 1;
        }

        private static bool IsEmailUserValid(string user)
        {
            return !user.NullOrEmpty();
        }

        private static bool IsEmailDomainValid(string domain)
        {
            string[] domainParts = domain.Split('.');
            return domainParts.Length > 1 && domainParts.All(domainPart => domainPart.Length > 0);
        }
    }
}
