﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Users.Suppliers;

namespace PersistentDatabaseStore
{
    public class SupplierPersistentStore: SuppliersDataProvider
    {

        public PersistentStoreContext PersistentStoreContext { get; set; }

        public bool ExistsSupplier(Supplier supplier)
        {
            try
            {
                var resultsFromStore = from s in PersistentStoreContext.Suppliers
                                       where s.Id == supplier.Id
                                       select s;
                return resultsFromStore.Count() > 0;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public Supplier GetSupplier(Supplier supplier)
        {
            IQueryable<Supplier> resultsFromStore;

            try
            {
                resultsFromStore = from s in PersistentStoreContext.Suppliers
                                   where s.Id == supplier.Id
                                   select s;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe el proveedor");
            return resultsFromStore.First();
        }
        public void ModifySupplier(Supplier supplierToModify, Supplier newSupplier)
        {
            Supplier realSupplier = GetSupplier(supplierToModify);
            try
            { 
                realSupplier.Name = newSupplier.Name;
                realSupplier.Overview = newSupplier.Overview;
                realSupplier.Rate = newSupplier.Rate;
                realSupplier.Phone = newSupplier.Phone;
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
    }
}
