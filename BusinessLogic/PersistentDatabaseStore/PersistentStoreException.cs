﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistentDatabaseStore
{

    [ExcludeFromCodeCoverage]
    public class PersistentStoreException : Exception
    {

        public PersistentStoreException() : base()
        {

        }

        public PersistentStoreException(string message) : base(message)
        {

        }

        public PersistentStoreException(string message, Exception innerException) : base(message, innerException)
        {

        }

    }

}
