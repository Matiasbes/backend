namespace PersistentDatabaseStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "FacebookId", c => c.String());
            AddColumn("dbo.Projects", "Location_Latitude", c => c.Double(nullable: false));
            AddColumn("dbo.Projects", "Location_Longitude", c => c.Double(nullable: false));
            DropColumn("dbo.Users", "LinkedInId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "LinkedInId", c => c.String());
            DropColumn("dbo.Projects", "Location_Longitude");
            DropColumn("dbo.Projects", "Location_Latitude");
            DropColumn("dbo.Users", "FacebookId");
        }
    }
}
