﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Projects;
using BusinessLogic.Users.Clients;

namespace PersistentDatabaseStore
{
    public class ClientPersistentStore: ClientsDataProvider
    {
        public PersistentStoreContext PersistentStoreContext { get; set; }

        public bool ExistsClient(Client client)
        {
            try
            {
                var resultsFromStore = from c in PersistentStoreContext.Clients
                                       where c.Id == client.Id
                                       select c;
                return resultsFromStore.Count() > 0;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public Client GetClient(Client client)
        {
            IQueryable<Client> resultsFromStore;

            try
            {
                resultsFromStore = from c in PersistentStoreContext.Clients
                                   where c.Id == client.Id
                                   select c;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe el cliente");
            return resultsFromStore.First();
        }

        public void ModifyClient(Client client, Client newClient)
        {
            Client realClient = GetClient(client);
            try
            {
                realClient.Name = newClient.Name;
                realClient.Phone = newClient.Phone;
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
    }
}
