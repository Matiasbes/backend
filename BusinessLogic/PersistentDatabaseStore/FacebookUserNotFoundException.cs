﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistentDatabaseStore
{

    [ExcludeFromCodeCoverage]
    public class FacebookUserNotFoundException : Exception
    {

        public FacebookUserNotFoundException() : base()
        {

        }

        public FacebookUserNotFoundException(string message) : base(message)
        {

        }

        public FacebookUserNotFoundException(string message, Exception innerException): base(message, innerException)
        {

        }
    }
}
