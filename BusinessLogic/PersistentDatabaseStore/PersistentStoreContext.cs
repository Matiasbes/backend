﻿using BusinessLogic.Authentication;
using BusinessLogic.Image;
using BusinessLogic.Projects;
using BusinessLogic.Users;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Degrees;
using BusinessLogic.Users.Suppliers.Jobs;
using BusinessLogic.Users.Suppliers.Portfolio;
using BusinessLogic.Users.Suppliers.Skills;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistentDatabaseStore
{

    [ExcludeFromCodeCoverage]
    public class PersistentStoreContext : DbContext
    {

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Degree> Degrees { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Session> Sessions { get; set; }
        public virtual DbSet<ProjectRating> ProjectRatings { get; set; }
        public virtual DbSet<PortfolioSample> PortfolioSamples { get; set; }
        public virtual DbSet<ImageDefinition> Images { get; set; }
        public virtual DbSet<DeviceId> DevicesIds { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>().ToTable("Clients");
            modelBuilder.Entity<Supplier>().ToTable("Suppliers");
            modelBuilder.Entity<ProjectRating>().ToTable("ProjectRatings");
            modelBuilder.Entity<ImageDefinition>().ToTable("Images");
            modelBuilder.Entity<Project>().HasMany<Supplier>(project => project.Suppliers).WithMany();
        }

    }
}
