﻿using BusinessLogic.Projects;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Image;
using Utilities;
using System.Device.Location;

namespace PersistentDatabaseStore
{
    public class ProjectPersistentStore : ProjectsDataProvider
    {
        public PersistentStoreContext PersistentStoreContext { get; set; }


        public List<Project> GetClientProjects(Client client)
        {
            try
            {
                var results = from p in PersistentStoreContext.Projects
                              where p.Client.Id == client.Id
                              select p;
                return results.ToList<Project>();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public List<Project> GetSupplierProjects(Supplier supplier)
        {
            try
            {
                var results = from p in PersistentStoreContext.Projects
                              where p.Suppliers.Any(theSupplier => theSupplier.Id == supplier.Id)
                              select p;
                return results.ToList<Project>();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public Project AddProject(Project project)
        {
            try
            {
                var results = from c in PersistentStoreContext.Clients
                              where c.Id == project.Client.Id
                              select c;
                Client client = results.ToList<Client>().First();
                project.Client = client;
                PersistentStoreContext.Projects.Add(project);
                PersistentStoreContext.SaveChanges();
                return project;

            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public bool ExistsProject(Project project)
        {
            try
            {
                var results = from p in PersistentStoreContext.Projects
                              where p.Id == project.Id
                              select p;
                return results.Count() > 0;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public Project GetProject(Project project)
        {
            IQueryable<Project> projectsFromStore;

            try
            {
                projectsFromStore = from p in PersistentStoreContext.Projects
                                    where p.Id == project.Id
                                    select p;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (projectsFromStore.Count() == 0)
                throw new ArgumentException("No existe el proyecto");
            return projectsFromStore.First();
        }

        public void ModifyProject(Project projectToModify, Project newProject)
        {
            Project realProjectToModify = GetProject(projectToModify);
            try
            {
                realProjectToModify.Title = newProject.Title;
                realProjectToModify.Description = newProject.Description;
                realProjectToModify.Budget = newProject.Budget;
                realProjectToModify.Client = newProject.Client;
                realProjectToModify.Suppliers = newProject.Suppliers;
                realProjectToModify.Status = newProject.Status;
                realProjectToModify.Deadline = newProject.Deadline;

                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void FinalizeProject(Project project, IList<ProjectRating> supplierRatings)
        {
            Project realProject = GetProject(project);

            realProject.Status = ProjectStatus.Finished;
            realProject.Ratings = new List<ProjectRating>();
            supplierRatings.ToList().ForEach(rating => {
                rating.Supplier = GetSupplier(rating.Supplier);
                realProject.Ratings.Add(rating);
            });

            try
            {
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        private Supplier GetSupplier(Supplier supplier)
        {
            IQueryable<Supplier> suppliersFromStore;

            try
            {
                suppliersFromStore = from f in PersistentStoreContext.Suppliers
                                       where f.Id == supplier.Id
                                       select f;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (suppliersFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return suppliersFromStore.First();
        }

        public void RateClient(Supplier supplier, Project project, int rating)
        {
            Project realProject = GetProject(project);
            try
            {
                ProjectRating projectRating = realProject.Ratings.ToList().Find(rate => rate.Supplier.Id == supplier.Id);
                projectRating.ClientRate = rating;
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public List<Project> GetProjects()
        {
            try
            {
                var results = from p in PersistentStoreContext.Projects
                              select p;
                return results.ToList<Project>();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public List<Project> GetProjectsByDistance(Double latitude, Double longitude, Double distance)
        {
            List<Project> possibleProjects = new List<Project>();
            List<Project> resultProjects = new List<Project>();

            var coord1 = new GeoCoordinate(latitude, longitude);
            

            
            try
            {
                var results = from p in PersistentStoreContext.Projects
                              select p;
                possibleProjects = results.ToList<Project>();
                foreach(Project proj in possibleProjects)
                {
                    var coord2 = new GeoCoordinate(proj.Location.Latitude, proj.Location.Longitude);
                    if((coord1.GetDistanceTo(coord2)/1000) < distance)
                    {
                        resultProjects.Add(proj);
                    }
                }
                return resultProjects;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void ProjectsApplay(Supplier supplier, IList<Project> projects)
        {
            foreach(Project p in projects)
            {
                Project realProject = GetProject(p);
                try
                {
                    ChangeProjectStatus(realProject, ProjectStatus.InProcess);
                    realProject.Suppliers.Add(GetSupplierFromStore(supplier));
                    PersistentStoreContext.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new PersistentStoreException("No se pudo procesar la solicitud", e);
                }
            }
        }
        private Supplier GetSupplierFromStore(Supplier supplier)
        {
            IQueryable<Supplier> supplierFromStore;

            try
            {
                supplierFromStore = from s in PersistentStoreContext.Suppliers
                                       where s.Id == supplier.Id
                                       select s;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (supplierFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return supplierFromStore.First();
        }

        private void ChangeProjectStatus(Project project, ProjectStatus newStatus)
        {
            project.Status = newStatus;
        }

        public void ModifyPhoto(Project project, ImageDefinition photo)
        {
            Project realProject= GetProject(project);
            try
            {
                if (realProject.Photo.IsNull())
                    realProject.Photo = photo;
                else
                {
                    realProject.Photo.ContentType = photo.ContentType;
                    realProject.Photo.FileName = photo.FileName;
                }
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
    }
}
