﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Users.Suppliers.Jobs;
using BusinessLogic.Users.Suppliers;

namespace PersistentDatabaseStore
{
    public class JobPersistentStore: JobsDataProvider
    {

        public PersistentStoreContext PersistentStoreContext { get; set; }

        public Job AddJob(Job job, Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            try
            {
                realSupplier.Jobs.Add(job);
                PersistentStoreContext.SaveChanges();
                return job;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        private Supplier GetSupplierFromStore(Supplier supplier)
        {
            IQueryable<Supplier> suppliersFromStore;

            try
            {
                suppliersFromStore = from f in PersistentStoreContext.Suppliers
                                       where f.Id == supplier.Id
                                       select f;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (suppliersFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return suppliersFromStore.First();
        }

        public Job GetJob(Job job)
        {
            IQueryable<Job> jobsFromStore;

            try
            {
                jobsFromStore = from j in PersistentStoreContext.Jobs
                                where j.Id == job.Id
                                select j;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (jobsFromStore.Count() == 0)
                throw new ArgumentException("No existe el historial de trabajo buscado");
            return jobsFromStore.First();
        }

        public IList<Job> GetJobsForSupplier(Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            return realSupplier.Jobs;
        }

        public Job ModifyJob(Job jobToModify, Job newJob)
        {
            Job realJobToModify = GetJob(jobToModify);
            try
            {
                realJobToModify.Enterprise = newJob.Enterprise;
                realJobToModify.Description = newJob.Description;
                realJobToModify.StartDate = newJob.StartDate;
                realJobToModify.EndDate = newJob.EndDate;
                realJobToModify.Title = newJob.Title;
                PersistentStoreContext.SaveChanges();
                return realJobToModify;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void RemoveJob(Job job)
        {
            Job realJobToRemove = GetJob(job);
            try
            {
                PersistentStoreContext.Jobs.Remove(realJobToRemove);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

    }
}
