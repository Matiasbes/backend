﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistentDatabaseStore
{
    public class SkillPersistentStore : SkillsDataProvider
    {
        public PersistentStoreContext PersistentStoreContext { get; set; }

        public Skill AddSkill(Skill skill, Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            try
            {
                realSupplier.Skills.Add(skill);
                PersistentStoreContext.SaveChanges();
                return skill;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        private Supplier GetSupplierFromStore(Supplier supplier)
        {
            IQueryable<Supplier> suppliersFromStore;

            try
            {
                suppliersFromStore = from f in PersistentStoreContext.Suppliers
                                       where f.Id == supplier.Id
                                       select f;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (suppliersFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return suppliersFromStore.First();
        }

        public Skill GetSkill(Skill skill)
        {
            IQueryable<Skill> skillsFromStore;

            try
            {
                skillsFromStore = from s in PersistentStoreContext.Skills
                                  where s.Id == skill.Id
                                  select s;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (skillsFromStore.Count() == 0)
                throw new ArgumentException("No existe el skill que estas buscando");
            return skillsFromStore.First();
        }

        public IList<Skill> GetSkillsForSupplier(Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            return realSupplier.Skills;
        }

        public Skill ModifySkill(Skill skillToModify, Skill newSkill)
        {
            Skill realSkillToModify = GetSkill(skillToModify);
            try
            {
                realSkillToModify.Title = newSkill.Title;
                PersistentStoreContext.SaveChanges();
                return realSkillToModify;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void RemoveSkill(Skill skill)
        {
            Skill realSkillToRemvoe = GetSkill(skill);
            try
            {
                PersistentStoreContext.Skills.Remove(realSkillToRemvoe);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

    }
}
