﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Users;
using BusinessLogic.Projects;
using BusinessLogic.Users.Clients;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Authentication;
using BusinessLogic.Image;
using Utilities;

namespace PersistentDatabaseStore
{
    public class UserPersistentStore : UsersDataProvider
    {
        public PersistentStoreContext PersistentStoreContext { get; set; }

        public User AddUser(User user)
        {
            try
            {
                PersistentStoreContext.Users.Add(user);
                PersistentStoreContext.SaveChanges();
                return user;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public bool ExistsUserWithEmail(string email)
        {
            try
            {
                var resultsFromStore = from u in PersistentStoreContext.Users
                                       where u.Email.Equals(email)
                                       select u;
                return resultsFromStore.Count() > 0;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public User GetUserWithCredentials(string email, string password)
        {
            IQueryable<User> resultsFromStore;

            try
            {
                resultsFromStore = from u in PersistentStoreContext.Users
                                   where u.Email.Equals(email) &&
                                   u.Password.Equals(password)
                                   select u;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe el usuario");
            return resultsFromStore.First();
        }


        public User GetUser(User user)
        {
            IQueryable<User> resultsFromStore;

            try
            {
                resultsFromStore = from u in PersistentStoreContext.Users
                                   where u.Id == user.Id
                                   select u;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe el usuario");
            return resultsFromStore.First();
        }

        private User GetUserWithEmail(string userEmail)
        {
            IQueryable<User> resultsFromStore;

            try
            {
                resultsFromStore = from u in PersistentStoreContext.Users
                                   where u.Email.Equals(userEmail)
                                   select u;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe usuario con este email. Deberá registrarse primero.");
            return resultsFromStore.First();
        }

        public User ChangePassword(User user, string newPassword)
        {
            User realUser = GetUser(user);
            try
            {
                realUser.Password = newPassword;
                PersistentStoreContext.SaveChanges();
                return realUser;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public Supplier GetSupplierFromUser(User user)
        {
            IQueryable<Supplier> resultsFromStore;

            try
            {
                resultsFromStore = from f in PersistentStoreContext.Suppliers
                                   where f.Id == user.Id
                                   select f;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return resultsFromStore.First();
        }

        public Client GetClientFromUser(User user)
        {
            IQueryable<Client> resultsFromStore;

            try
            {
                resultsFromStore = from c in PersistentStoreContext.Clients
                                   where c.Id == user.Id
                                   select c;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No existe el cliente");
            return resultsFromStore.First();
        }
        public void RemoveSession(Session session)
        {
            Session sessionToRemove = GetSession(session);
            try
            {
                PersistentStoreContext.Sessions.Remove(sessionToRemove);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        private Session GetSession(Session session)
        {
            IQueryable<Session> resultsFromStore;

            try
            {
                resultsFromStore = from s in PersistentStoreContext.Sessions
                                   where s.Token.Equals(session.Token)
                                   select s;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new ArgumentException("No se pudo cerrar la sesión");
            return resultsFromStore.First();
        }

        public User GetUserFromSession(Session session)
        {
            try
            {
                if (PersistentStoreContext.Users.Any<User>(u => u.Sessions.Any<Session>(s => s.Token.Equals(session.Token))))
                {
                    User user = PersistentStoreContext.Users.FirstOrDefault<User>(u => u.Sessions.Any<Session>(s => s.Token.Equals(session.Token)));
                    return user;
                }
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
            throw new ArgumentException("No se encontró usuario para la sesión");
        }
        public void AddSessionToUser(User user, Session session)
        {
            User realUser = GetUser(user);
            try
            {
                realUser.Sessions.Add(session);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
        public User GetUserWithFacebookId(string facebookId)
        {
            IQueryable<User> resultsFromStore;

            try
            {
                resultsFromStore = from u in PersistentStoreContext.Users
                                   where u.FacebookId.Equals(facebookId)
                                   select u;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (resultsFromStore.Count() == 0)
                throw new FacebookUserNotFoundException("No hay ninguna cuenta asociada con ese usuario de Facebook");
            return resultsFromStore.First();
        }

        public User SyncUserWithFacebook(string userEmail, string facebookId)
        {
            User realUser = GetUserWithEmail(userEmail);
            try
            {
                realUser.FacebookId = facebookId;
                PersistentStoreContext.SaveChanges();
                return realUser;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
        public int GetUserRating(User user)
        {
            try
            {
                Tuple<int, int> projectRatings = CalculateRating(user);
                return projectRatings.Item2 == 0 ? 0 : projectRatings.Item1 / projectRatings.Item2;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

        }
        private Tuple<int, int> CalculateRating(User user)
        {
            int amountOfRatings = 0;
            IEnumerable<int> ratings = GetUserProjects(user).Select(project =>
            {
                Tuple<int, int> projectRating = GetProjectRating(user, project);
                amountOfRatings += projectRating.Item2;
                return projectRating.Item1;
            });
            int totalRatings = ratings.ToList().Aggregate(0, (accumulator, rate) => accumulator + rate);
            return Tuple.Create(totalRatings, amountOfRatings);
        }
        private List<Project> GetUserProjects(User user)
        {
            return PersistentStoreContext.Projects.Where(work => work.Client.Id == user.Id || work.Ratings.Any(rate => rate.Supplier.Id == user.Id)).ToList();
        }

        private Tuple<int, int> GetProjectRating(User user, Project project)
        {
            if (project.Client.Id == user.Id)
                return GetProjectRatingForClient(user, project);
            return GetProjectRatingForSupplier(user, project);
        }

        private Tuple<int, int> GetProjectRatingForClient(User user, Project project)
        {
            int amountOfRatings = project.Ratings.Where(rate => rate.ClientRate > 0).Count();
            return Tuple.Create(project.Ratings.Where(rate => rate.ClientRate > 0).ToList().Aggregate(0, (accumulator, rate) => accumulator + rate.ClientRate), amountOfRatings);
        }

        private Tuple<int, int> GetProjectRatingForSupplier(User user, Project project)
        {
            return Tuple.Create(project.Ratings.First(rate => rate.Supplier.Id == user.Id).SupplierRate, 1);
        }

        public void ModifyProfilePhoto(User user, ImageDefinition profilePhoto)
        {
            User realUser = GetUser(user);
            try
            {
                if (realUser.ProfilePhoto.IsNull())
                    realUser.ProfilePhoto = profilePhoto;
                else
                {
                    realUser.ProfilePhoto.ContentType = profilePhoto.ContentType;
                    realUser.ProfilePhoto.FileName = profilePhoto.FileName;
                }
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
        public void AddDeviceId(User user, DeviceId deviceId)
        {
            User realUser = GetUser(user);
            try
            {
                if (realUser.DevicesIds.Any(theDeviceId => theDeviceId.DeviceToken.Equals(deviceId.DeviceToken)))
                    return;
                realUser.DevicesIds.Add(deviceId);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }
    }
}
