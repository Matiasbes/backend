﻿using BusinessLogic.Image;
using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Portfolio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace PersistentDatabaseStore
{
    public class PortfolioSamplePersistentStore : PortfolioSamplesDataProvider
    {

        public PersistentStoreContext PersistentStoreContext { get; set; }

        public PortfolioSample AddPortfolioSample(PortfolioSample portfolioSample, Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            try
            {
                realSupplier.Portfolio.Add(portfolioSample);
                PersistentStoreContext.SaveChanges();
                return portfolioSample;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        private Supplier GetSupplierFromStore(Supplier supplier)
        {
            IQueryable<Supplier> suppliersFromStore;

            try
            {
                suppliersFromStore = from f in PersistentStoreContext.Suppliers
                                       where f.Id == supplier.Id
                                       select f;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (suppliersFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return suppliersFromStore.First();
        }

        public PortfolioSample GetPortfolioSample(PortfolioSample portfolioSample)
        {
            IQueryable<PortfolioSample> portfolioSampleFromStore;

            try
            {
                portfolioSampleFromStore = from p in PersistentStoreContext.PortfolioSamples
                                           where p.Id == portfolioSample.Id
                                           select p;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (portfolioSampleFromStore.Count() == 0)
                throw new ArgumentException("No existe el item del portfolio que estas buscando");
            return portfolioSampleFromStore.First();
        }

        public IList<PortfolioSample> GetPortfolioSamplesForSupplier(Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            return realSupplier.Portfolio;
        }

        public PortfolioSample ModifyPortfolioSample(PortfolioSample portfolioSampleToModify, PortfolioSample newPortfolioSample)
        {
            PortfolioSample realPortfolioSampleToModify = GetPortfolioSample(portfolioSampleToModify);
            try
            {
                realPortfolioSampleToModify.Title = newPortfolioSample.Title;
                realPortfolioSampleToModify.Description = newPortfolioSample.Description;
                realPortfolioSampleToModify.Date = newPortfolioSample.Date;
                realPortfolioSampleToModify.Url = newPortfolioSample.Url;
                PersistentStoreContext.SaveChanges();
                return realPortfolioSampleToModify;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void RemovePortfolioSample(PortfolioSample portfolioSample)
        {
            PortfolioSample realPortfolioSampleToRemove = GetPortfolioSample(portfolioSample);
            try
            {
                PersistentStoreContext.PortfolioSamples.Remove(realPortfolioSampleToRemove);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void ModifyPhoto(PortfolioSample portfolioSample, ImageDefinition photo)
        {
            PortfolioSample realPortfolioSample = GetPortfolioSample(portfolioSample);
            try
            {
                if (realPortfolioSample.Photo.IsNull())
                    realPortfolioSample.Photo = photo;
                else
                {
                    realPortfolioSample.Photo.ContentType = photo.ContentType;
                    realPortfolioSample.Photo.FileName = photo.FileName;
                }
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

    }

}
