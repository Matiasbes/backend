﻿using BusinessLogic.Users.Suppliers;
using BusinessLogic.Users.Suppliers.Degrees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistentDatabaseStore
{
    public class DegreePersistentStore : DegreesDataProvider
    {
        public PersistentStoreContext PersistentStoreContext { get; set; }

        public Degree AddDegree(Degree degree, Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            try
            {
                realSupplier.Degrees.Add(degree);
                PersistentStoreContext.SaveChanges();
                return degree;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        private Supplier GetSupplierFromStore(Supplier supplier)
        {
            IQueryable<Supplier> suppliersFromStore;

            try
            {
                suppliersFromStore = from f in PersistentStoreContext.Suppliers
                                       where f.Id == supplier.Id
                                       select f;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (suppliersFromStore.Count() == 0)
                throw new ArgumentException("No existe el supplier");
            return suppliersFromStore.First();
        }

        public Degree GetDegree(Degree degree)
        {
            IQueryable<Degree> degreesFromStore;

            try
            {
                degreesFromStore = from d in PersistentStoreContext.Degrees
                                   where d.Id == degree.Id
                                   select d;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }

            if (degreesFromStore.Count() == 0)
                throw new ArgumentException("No existe la certificacion buscada");
            return degreesFromStore.First();
        }

        public IList<Degree> GetDegreesForSupplier(Supplier supplier)
        {
            Supplier realSupplier = GetSupplierFromStore(supplier);
            return realSupplier.Degrees;
        }

        public Degree ModifyDegree(Degree degreeToModify, Degree newDegree)
        {
            Degree realDegreeToModify = GetDegree(degreeToModify);
            try
            {
                realDegreeToModify.Institution = newDegree.Institution;
                realDegreeToModify.Description = newDegree.Description;
                realDegreeToModify.StartDate = newDegree.StartDate;
                realDegreeToModify.EndDate = newDegree.EndDate;
                realDegreeToModify.Title = newDegree.Title;
                PersistentStoreContext.SaveChanges();
                return realDegreeToModify;
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

        public void RemoveDegree(Degree degree)
        {
            Degree realDegreeToRemove = GetDegree(degree);
            try
            {
                PersistentStoreContext.Degrees.Remove(realDegreeToRemove);
                PersistentStoreContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new PersistentStoreException("No se pudo procesar la solicitud", e);
            }
        }

    }
}
